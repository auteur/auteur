Readme file for auteur (from the French for "author").
written 
29 December 2010 - 26 Jan 2011 by Neil Wallace <rowinggolfer@googlemail.com>


Please visit http://auteur-editor.info for community and bug trackers, or join us in #auteur on freenode IRC 




This application began with an idea by not_klaatu in his "gnu world order" oggcast http://www.thebadapples.info/

It is a video editor which harnesses the power of mplayer to edit video.

It hopefully embraces the unix philosophy of "do one thing and do it well".

DEPENDECIES.
mplayer and mencoder    http://www.mplayerhq.hu/
qt4                     http://qt.nokia.com/downloads/
python-qt4              http://www.riverbankcomputing.co.uk/software/pyqt/intro


INSTALL
auteur uses python distutils for installation

~$ sudo python setup.py install



USEAGE.
~$ auteur [options]

if no options are set, auteur will start in editing mode. This is probably what you require initially
The editor generates an xml file which describes your vision of the video - for the remainder of this README, such as file is called "foo.xml"

OPTIONS

    <filename>
            maps to --edit <filename> 
   
            EXAMPLE
            ./auteur foo.xml         
            
    --edit <filename> OR -e <filename>      
    
            continues editing where you left off.
            EXAMPLE (all are equivalent)
            ./auteur foo.xml
            ./auteur -edit foo.xml
            ./auteur -e foo.xml

    --play <filename> OR -p <filename>        
          
            shows you a preview of your edit.
            EXAMPLE 
            ./auteur -play foo.xml
            ./auteur -p foo.xml
          
    --render <filename> OR -r <filename>
            
            renders your edit            
            EXAMPLE
            ./auteur -render foo.xml
            ./auteur -r foo.xml

            
            







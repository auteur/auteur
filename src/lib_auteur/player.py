#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import subprocess

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../"))

from PyQt4 import QtGui
from lib_auteur.components import DataModel
from lib_auteur.components import ScreenWidget, VideoControls

class Player(ScreenWidget):
    def __init__(self, model, mplayer_opts, parent=None):
        ScreenWidget.__init__(self, mplayer_opts, parent)
        
        controls = VideoControls()
        self.set_controls(controls)
        
        self.player.start(self._Xscreen.winId(), 
                        model.preview_commands, 
                        source_review=False)
        
        self.controls.enable()
    
def main(filename, mplayer_opts=[]):
    import gettext
    gettext.install("auteur")
    
    app = QtGui.QApplication([])
    
    model = DataModel()
    model.load(filename)
    
    player = Player(model, mplayer_opts)
    player.sub_window.show()

    app.exec_()

if __name__ == "__main__":
    main("test_project/test_project.xml")
    
    
    
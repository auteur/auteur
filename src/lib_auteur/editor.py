#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, subprocess, sys

from PyQt4 import QtCore, QtGui 

if __name__ == "__main__":
    sys.path.insert(0, os.path.abspath("../"))
    
from lib_auteur.components import *
from lib_auteur.settings import QAuteurSettings
        
class MainApp(QtGui.QMainWindow, EditorMenu):
    def __init__(self, mplayer_opts=[], mencoder_opts=[], verbose=False, 
    parent=None):
    
        QtGui.QMainWindow.__init__(self, parent)
        EditorMenu.__init__(self)
        
        self.settings = QAuteurSettings(self)
        self.settings.setValue("verbose", verbose)
        self.setWindowTitle("auteur powered by mplayer/mencoder")
        
        self.setWindowIcon(QtGui.QIcon(":images/handInFilm.svg"))
        
        self.mplayer_opts = mplayer_opts
        self.mencoder_opts = mencoder_opts
        
        for toolbar in self.tool_bars:
            self.addToolBar(toolbar)
        
        self.menu_toolbar = self.createPopupMenu()
        self.insert_toolbar_menu(self.menu_toolbar)
        
        ## the data model
        self.data_model = DataModel()
        
        ## the screen
        self.screen_widget = ScreenWidget(mplayer_opts, self)
        self.player = self.screen_widget.player
        self.video_controls = VideoControls()
        self.video_controls.setObjectName("video_controls") #for QSettings
        self.video_controls.slider.setModel(self.data_model)
        self.data_model.attach_view(self.video_controls.slider)
        
        ## data view widgets
        self.data_widget = DataWidget()
        self.data_widget.setWindowTitle(_("DATA"))
        self.xml_widget = self.data_widget.xml_widget
        self.xml_widget.set_model(self.data_model)
        
        ## mencoder line
        self.mencoder_widget = MencoderWidget()
        self.mencoder_widget.set_model(self.data_model)
        
        ## sources
        self.sources_widget = SourcesWidget()
        self.sources_widget.set_model(self.data_model)
        
        ##clips
        self.clips_widget = ClipsWidget()
        self.clips_widget.set_model(self.data_model)
        
        ##stills
        self.still_image_widget = StillImageWidget()
        self.still_image_widget.set_model(self.data_model)
        
        ##timeline
        self.timeline_widget = TimeLineWidget()
        self.timeline_widget.set_model(self.data_model)
        
        ##workflow
        self.workflow_widget = WorkflowWidget(self.data_model)
        
        ## let the model know we need to be informed of changes!
        ## other widgets attach during their setModel function
        self.data_model.attach_view(self) #to enable undo action
        self.data_model.attach_view(self.workflow_widget) 
        
        self.connect_menu_signals()
        self.connect_widget_signals()
        
        self.action_docked.setChecked(self.settings.is_docked)
        self.action_multi.setChecked(self.settings.is_multi_window)
        
        self.components = ( self.workflow_widget,
                            self.data_widget,
                            self.mencoder_widget, 
                            self.clips_widget, 
                            self.sources_widget, 
                            self.screen_widget,
                            self.still_image_widget,
                            self.timeline_widget)
                            
        for component in self.components:
            self.insert_view_action(component.action)
        
        self.init_ui()
            
    def save_settings(self):
        print ("saving settings!")
        if self.settings.is_docked:
            print ("docked settings")
            self.settings.setValue("docked_state", self.saveState())
            self.settings.setValue("docked_geometry", self.saveGeometry())
        else:
            print ("multi settings")
            self.settings.setValue("multi_state", self.saveState())
            self.settings.setValue("multi_geometry", self.saveGeometry())
            for component in self.components:
                if (component.has_sub_window and 
                component.sub_window.isVisible()):
                    component.sub_window.save_settings()
        for component in self.components:
            self.settings.setValue("%s_visible"% component.objectName(), 
                component.action.isChecked())
        
        
    def connect_menu_signals(self):
        
        self.action_new_workspace.triggered.connect(self.new_workspace)
        self.action_load_workspace.triggered.connect(self.load_workspace)
        self.action_save_workspace.triggered.connect(self.save_workspace)        
        
        self.action_get_sources.triggered.connect(self.load_sources)
        
        self.action_undo.triggered.connect(self.undo)
        self.action_redo.triggered.connect(self.redo)
        
        self.connect(self.action_quit, QtCore.SIGNAL("triggered()"),
        QtGui.QApplication.instance().closeAllWindows)

        self.action_fullscreen.triggered.connect(self.fullscreen)

        self.action_about.triggered.connect(self.show_about)
        self.action_license.triggered.connect(self.show_license)

        self.connect(self.action_about_qt, QtCore.SIGNAL("triggered()"),
            QtGui.qApp, QtCore.SLOT("aboutQt()"))

        self.action_help.triggered.connect(self.show_help)

        self.action_default_layout.triggered.connect(self.restore_default_ui)
        self.connect(self, QtCore.SIGNAL("update ui"), self.setup_ui)

    def sizeHint(self):
        return QtCore.QSize(600,400)

    def connect_widget_signals(self):
        
        ## datawidget
        self.data_widget.save_button.clicked.connect(
            self.action_save_workspace.trigger)        
        self.data_widget.load_button.clicked.connect(
            self.action_load_workspace.trigger)
        
        ## signals from the work_flow_widget
        self.connect(self.workflow_widget, QtCore.SIGNAL("load sources"),
            self.load_sources)
        self.connect(self.workflow_widget, QtCore.SIGNAL("render"),
            self.render)
        
        ## signals from the sources widget
        self.sources_widget.add_button.clicked.connect(
            self.load_sources)
        self.connect(self.sources_widget, QtCore.SIGNAL("source chosen"),
            self.screen_widget.load_source)
        self.connect(self.sources_widget,
            QtCore.SIGNAL("dragging clip"), self.show_dropzones)

        ## signals from the clip widget and time_line
        for widg in (self.clips_widget, self.timeline_widget):
            self.connect(widg, QtCore.SIGNAL("clip selected"),
                self.load_clip)
            self.connect(widg, QtCore.SIGNAL("delete clip"), self.delete_clip)
            self.connect(widg, QtCore.SIGNAL("render"), self.render)
            self.connect(widg, QtCore.SIGNAL("preview"), self.preview)

        ## signals from the player          
        self.connect(self.player.signaller, 
            QtCore.SIGNAL("file changed"), self.data_model.playing_file_update)
        self.connect(self.player.signaller, 
            QtCore.SIGNAL("split position"), self.split_clip)
        
        ## signals from the controls        
        
        self.connect(self.video_controls.slider,
            QtCore.SIGNAL("dragging clip"), self.show_dropzones)
     
    def init_ui(self):
        '''
        called at startup - and never again!
        ui is always initiated in docked mode.
        ''' 
        self.screen_widget.action.setEnabled(False)
        self.screen_widget.action.setChecked(True)
                
        self.addDockWidget(
            QtCore.Qt.RightDockWidgetArea, self.workflow_widget.dock_widget)

        self.addDockWidget(
            QtCore.Qt.BottomDockWidgetArea, self.data_widget.dock_widget)

        self.addDockWidget(
                QtCore.Qt.LeftDockWidgetArea, self.sources_widget.dock_widget)

        for widget in ( self.clips_widget.dock_widget,
                        self.mencoder_widget.dock_widget,
                        self.still_image_widget.dock_widget,
                        self.timeline_widget.dock_widget
                        ):

            self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, widget)
            self.tabifyDockWidget(self.data_widget.dock_widget, widget)
        
        self.screen_widget.set_native(self.settings.native_mplayer)
        
        if not self.settings.native_mplayer:
            self.screen_widget.set_controls(self.video_controls)
        
        if self.settings.is_docked:
            self.setCentralWidget(self.screen_widget)
            self.restore_state()
        else:
            print ("converting to MDI ui")
            self.multi_ui()
        
    def setup_ui(self):
        '''
        toggle the gui between docked and multi
        '''
        print ("setup_ui called")
        if self.action_docked.isChecked():
            print ("converting to docked ui")
            self.docked_ui()
        else:
            print ("converting to MDI ui")
            self.multi_ui()
    
    def restore_state(self):
        try:
            self.restoreState(self.settings.window_state)
            self.restoreGeometry(self.settings.geometry)
            for component in self.components:
                val = self.settings.is_component_action_checked(component)
                component.action.setChecked(val)
            
        except KeyError:
            print ("no previous window state found")
                
    def docked_ui(self):
        '''
        user has chosen a docked ui
        '''
        self.settings.setValue("is_docked", True)
        self.settings.setValue("multi_state", self.saveState())
        self.settings.setValue("multi_geometry", self.saveGeometry())
        self.settings.sync()
          
        for toolbar in self.tool_bars:
            self.removeToolBarBreak(toolbar)
          
        for component in self.components:
            if component == self.screen_widget:
                self.setCentralWidget(self.screen_widget)
                component.delete_sub_window()
            else:
                self.restoreDockWidget(component.dock_widget)
                component.dock_widget.setFloating(False)

        self.restore_state()
        
        self.workflow_widget.action.setEnabled(True)
        self.screen_widget.action.setEnabled(False)
        self.screen_widget.action.setChecked(True)
        
        for component in self.components:
            component.show_widget(component.action.isChecked())
        
    def multi_ui(self):
        print ("setup_multi_ui")
        
        for component in self.components:
            if component.has_dock:
                component.dock_widget.setFloating(False)
        
        self.settings.setValue("is_docked", False)
        self.settings.setValue("docked_state", self.saveState())
        self.settings.setValue("docked_geometry", self.saveGeometry())
        self.settings.sync()
        
        #next line prevents widget deletion when central widg is changed
        self.screen_widget.sub_window 
        self.screen_widget.action.setEnabled(True)
        
        for toolbar in self.tool_bars:
            self.addToolBar(toolbar)
            self.addToolBarBreak()
        
        self.resize(140,500)
        self.move(0,0)
        self.raise_()
        
        self.restore_state()
        
        self.setCentralWidget(self.sources_widget)
                        
        for component in self.components:
            if component == self.sources_widget:
                component.action.setEnabled(False)
                component.action.setChecked(True)
            else:
                sw = component.sub_window
                
        self.screen_widget.menu_checked = True        
            
        for component in self.components:
            component.show_widget(component.action.isChecked())
        
            self.removeDockWidget(component.empty_dock)
        
    def restore_default_ui(self):
        for action in self.added_actions:
            action.setChecked(False)
            action.trigger()
    
    def show_dropzones(self):
        if not (self.clips_widget.isVisible() or 
        self.timeline_widget.isVisible()) :
            self.clips_widget.show_widget(True)
            
        self.clips_widget.flash_drop()
        self.timeline_widget.flash_drop()
        
    def load_clip(self, clip=None):
        print ("load clip", clip)
        if clip and self.sender() == self.timeline_widget:
            self.clips_widget.select_clip_from_id(clip.id)
        self.data_model.currently_playing_clip = clip
        self.screen_widget.load_clip(clip)
        
    def split_clip(self, time_stamp):
        '''
        either mark a source.. or split a clip
        '''
        if self.screen_widget.is_playing_a_source:
           self.video_controls.slider.mark(time_stamp) 
        else:
            selected_clip = self.clips_widget.selected_clip
            if selected_clip and self.settings.split_warning_accepted:
                self.screen_widget.stop()
                clip_id = self.data_model.split_clip(selected_clip, time_stamp)
                self.clips_widget.select_clip_from_id(clip_id)
    
    def delete_clip(self, clip):
        print ("delete clip", clip)
        if (clip and
        QtGui.QMessageBox.question(self, _("confirm"),
        "%s %s"% (_("delete clip?"), clip.id),
        QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
        QtGui.QMessageBox.Ok) == QtGui.QMessageBox.Ok):

            self.screen_widget.stop()
            self.data_model.delete_clip(clip)

    def show_toolbar(self):
        if self.action_show_toolbar.isChecked():
            self.video_controls.show()
        else:
            self.video_controls.hide()
        self.menu_toolbar.setEnabled(self.video_controls.isVisible())

    def show_about(self):
        dl = AboutAuteurDialog(self)
        dl.show()

    def show_help(self):
        dl = AboutAuteurDialog(self)
        dl.show()

    def show_license(self):
        '''
        attempts to read and show the license text
        from file COPYRIGHT.txt in the apps directory
        on failure, gives a simple message box with link.
        '''
        message = '''
        GPLv3 - see <a href='http://www.gnu.org/licenses/gpl.html'>
        http://www.gnu.org/licenses/gpl.html</a>'''
        try:
            f = open("../COPYING.txt")
            data = f.read()
            f.close()

            dl = QtGui.QDialog(self)
            dl.setWindowTitle(_("License"))
            dl.setFixedSize(400, 400)

            layout = QtGui.QVBoxLayout(dl)

            buttonBox = QtGui.QDialogButtonBox(dl)
            buttonBox.setOrientation(QtCore.Qt.Horizontal)
            buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)

            te = QtGui.QTextBrowser()
            te.setText(data)

            label = QtGui.QLabel(message)
            label.setWordWrap(True)

            layout.addWidget(te)
            layout.addWidget(label)
            layout.addWidget(buttonBox)

            buttonBox.accepted.connect(dl.accept)

            dl.exec_()
        except IOError:
            QtGui.QMessageBox.information(self, _("License"), message)

    def fullscreen(self):
        if self.action_fullscreen.isChecked():
            self.setWindowState(QtCore.Qt.WindowFullScreen)
        else:
            self.setWindowState(QtCore.Qt.WindowNoState)

    def closeEvent(self, event=None):
        '''
        re-implement the close event of QtGui.QMainWindow, and check the user
        really meant to do this.
        '''
        if (
        self.data_model.is_dirty and 

        QtGui.QMessageBox.question(self, _("Confirm"), 
        _("You have unsaved changes to the workbench xml") + "<hr />" +
        _("Are you sure you want to quit?"),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        QtGui.QMessageBox.Yes) == QtGui.QMessageBox.No
        ):
            event.ignore()
        else:
            self.player.quit()
            self.save_settings()
            
            for component in self.components:
                if component.has_sub_window:
                    component.sub_window.close()
                    
                
    def load_sources(self, *args):
        vids = "*.asf *.au *.avi *.dv *.flv *.mkv *.mov *.mp4 "+\
        "*.mpeg *.mpg *.nut *.ogv *.ogg *.rm *.swf"
        images = "*.png *.xpm *.jpg *.gif *.jpeg"
        audio = "*.oga *.ogg *.mp3 *.wav"
        
        #Don't allow audio yet!!
        
        #file_select = 'Media (%s);;Videos (%s);;Images (%s);;Audio (%s);;'% (
        #    vids+images+audio, vids, images, audio)
        
        file_select = 'Media (%s);;Videos (%s);;Images (%s);;'% (
            vids+images, vids, images)            
        
        filenames = QtGui.QFileDialog.getOpenFileNames(self,
        "select media sources", self.settings.video_path, 
        file_select + 'All Files (*.*)')
        
        QtGui.QApplication.instance().setOverrideCursor(QtCore.Qt.WaitCursor)
        
        self.sources_widget.show_widget(True)
        errors_ = []
        for filename in filenames:
            filename = str(filename) 
            self.settings.setValue("video_path", 
                    os.path.dirname(filename))
            try:
                self.data_model.add_source_file(filename)
            except TypeError as e:
                errors_.append(e)
                
            QtGui.QApplication.instance().processEvents()
        QtGui.QApplication.instance().restoreOverrideCursor()

        if errors_:
            e_string = ""
            for error in errors_:
                e_string += "<li>%s</li>"% error
            QtGui.QMessageBox.warning(self, _("Errors"), 
            "%s<hr /><ul>%s</ul><hr />%s"% (
            _("The following files were unrecognised"), e_string,
            _("sources must be video, audio or image files")))
            
    def new_workspace(self):
        if (
        self.data_model.is_dirty and 
        
        QtGui.QMessageBox.question(self, _("confirm"),
        _("Abandon Changes and Create a new workbench?"),
        QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
        QtGui.QMessageBox.Cancel) == QtGui.QMessageBox.Cancel):
            return
        self.data_model.new_workspace()

    def load_workspace(self, filename=False):
        if filename in (False, None, ""):
            filename = QtGui.QFileDialog.getOpenFileName(self,
            "select a workspace", self.settings.workspace_path, 
            "auteur xml files (*.xml)")

        if filename != "":
            filename = str(filename).replace(" ", "\ ")
            self.settings.setValue("workspace_path", os.path.dirname(filename))
            try:
                self.data_model.load(filename)
            except IOError as e:
                QtGui.QMessageBox.warning(self, _("whoops"),
                 "%s<hr />%s"% (
                 _("Your project file isn't recognised by Auteur"), e))
                
    def save_workspace(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,
        "Save Workspace", self.settings.workspace_path, 
        "auteur xml files (*.xml)")

        if filename != "":
            filename = str(filename).replace(" ", "\ ")
            self.settings.setValue("workspace_path", os.path.dirname(filename))
            if not filename.endswith(".xml"):
                filename += ".xml"
            result, error = self.data_model.save_file(filename)
            
            if result:
                QtGui.QMessageBox.information(self, _("sucess"), 
                _("workspace saved sucessfully"))
                
            else:
                QtGui.QMessageBox.warning(self, _("failure"), 
                error)
        
    def update_data(self):
        '''
        the model is calling this something has changed something of note
        '''
        self.action_undo.setEnabled(self.data_model.undo_available)
        self.action_redo.setEnabled(self.data_model.redo_available)
        
        if self.data_model.has_sources:
            self.screen_widget.show_message2()
        else:
            self.screen_widget.show_message1()
        
    def undo(self):
        self.data_model.undo()
        
    def redo(self):
        self.data_model.redo()

    def render(self):
        '''
        render the project
        '''        
        if self.data_model.clips() == []:
            QtGui.QMessageBox.information(self.sender(), "whoops", 
            _("nothing to render"))
            return
        
        commands = ["auteur", "--render", self.data_model.tempfile]
        
        if self.mencoder_opts:
            commands.extend(["--mencoder"])
            commands.extend([self.mencoder_opts]) 
        
        print(commands)
        try:
            subprocess.Popen(commands).pid
        except OSError:
            message = ("whoops.. misunderstood system call\n" 
                    "is auteur on your sys path?\n"
                    "if you are running a non-installed version\n"
                    "please put a symlink in /usr/local/bin")
            QtGui.QMessageBox.warning(self.sender(), "error", message)
    
    def preview(self):
        if self.data_model.clips() == []:
            QtGui.QMessageBox.information(self.sender(), "whoops", 
            _("nothing to play"))
            return
        self.data_model.currently_playing_clip=None
        self.screen_widget.play_preview(self.data_model)
        
    def old_preview(self):
        '''
        #######################################################################
        ## OLD CODE - used to start a separate process for this              ##
        #######################################################################
        '''
        try:
            subprocess.Popen(["auteur", "--play", 
                self.data_model.tempfile]).pid
        except OSError:
            message = ("whoops.. misunderstood system call\n" 
                    "is auteur on your sys path?\n"
                    "if you are running a non-installed version\n"
                    "please put a symlink in /usr/local/bin")
            QtGui.QMessageBox.warning(self.sender(), "error", message)
    
def main(filename=None, mplayer_opts=[], mencoder_opts=[], verbose=False):
    def load_workspace():
        editor.load_workspace(filename)
    app = QtGui.QApplication(sys.argv)
    app.setOrganizationName("q-auteur")
    app.setApplicationName("editor")
    
    editor = MainApp(mplayer_opts, mencoder_opts, verbose)
    editor.show()
    if filename:
        QtCore.QTimer.singleShot(0, load_workspace)

    sys.exit(app.exec_())
    
            
if __name__ == "__main__":
    
    import gettext
    gettext.install("auteur")
    
    main()

#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtGui, QtCore

class ClipLabel(QtGui.QLabel):
    def __init__(self, parent = None):
        QtGui.QLabel.__init__(self, parent)
        self.setMouseTracking(True)
        self.mouse_over = False
        self.is_selected = False
        self.clip = None
        
    def sizeHint(self):
        return QtCore.QSize(80,80)
        
    def mouseMoveEvent(self, event):
        if self.is_selected:
            self.mouse_over = False
        else:
            self.mouse_over = True
            self.update()
        
    def mousePressEvent(self, event):
        self.is_selected = True
        self.mouse_over = False
        if event.button() == 2: # right clicked
            self.emit(QtCore.SIGNAL("right clicked"), event.globalPos())
        else:
            self.emit(QtCore.SIGNAL("clicked"))
        self.update()
    
    def select(self, emit_click = True):
        self.is_selected = True
        if emit_click:
            self.emit(QtCore.SIGNAL("clicked"))
        self.update()
        
    def leaveEvent(self, event):
        self.mouse_over = False
        self.update()
            
    def sizeHint(self):
        return QtCore.QSize(80, 80)
        
    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawPixmap(self.rect(), self.pixmap())
        option = QtGui.QTextOption()
        option.setAlignment(QtCore.Qt.AlignCenter)
        painter.setPen(QtGui.QColor("black"))
        font = painter.font()
        font.setBold(True)
        font.setPointSize(28)
        painter.setFont(font)
        id = self.clip.id.lstrip("0")
        #painter.rotate(90)
        painter.drawText(QtCore.QRectF(self.rect()), id, option)

        painter.save()
        painter.translate(1,1)
        painter.setPen(QtGui.QColor("red"))
        painter.drawText(QtCore.QRectF(self.rect()), id, option)
        painter.restore()
        
        if self.clip.media_type == self.clip.VIDEO_MEDIA:
            painter.fillRect(QtCore.QRect(0,0,8,self.height()), 
                QtGui.QColor("black"))
                
            painter.fillRect(QtCore.QRect(self.width()-8,0,8,self.height()), 
                QtGui.QColor("black"))
            
            y = 2
            while y < self.height():
                
                painter.fillRect(QtCore.QRect(2,y,5,5), QtGui.QColor("white"))
                painter.fillRect(QtCore.QRect(self.width()-6,y,5,5), 
                    QtGui.QColor("white"))
                
                y+=10

        if self.is_selected:
            painter.save()
            pen = QtGui.QPen(QtGui.QColor("blue"), 2)
            painter.setPen(pen)
            painter.drawRect(self.rect().adjusted(1,1,-1,-1)) 
            painter.restore()
                       
        if self.mouse_over:
            painter.save()
            pen = QtGui.QPen(QtGui.QColor("aqua"), 2)
            painter.setPen(pen)
            painter.drawRect(self.rect().adjusted(1,1,-1,-1))
            painter.restore()
            
        if not self.clip.in_timeline:
            painter.save()
            pen = QtGui.QPen(QtGui.QColor("red"), 2)
            painter.setPen(pen)
            painter.drawLine(self.rect().topLeft(), self.rect().bottomRight())
            painter.restore()
            
 
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
        
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))
    from lib_auteur.components import DataModel
    
    model = DataModel() 
    model.load("../test_project/test_project.xml")
    
    clip_label = ClipLabel()
    clip = model.clips()[1]
    clip_label.clip = clip
    clip_label.setPixmap(QtGui.QPixmap(clip.image))
    clip_label.show()
    
    app.exec_()    

if __name__ == "__main__":
    _test_code()

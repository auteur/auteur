#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import webbrowser
from PyQt4 import QtCore, QtGui 

try:
    raw_input
    PYTHON3 = False
except NameError:
    PYTHON3 = True


def decode_byte_array(arg):
    if PYTHON3:
        return str(arg, encoding="utf8")
    else:
        return unicode(arg)

class AboutAuteurDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(_("About Auteur"))
        
        icon_label = QtGui.QLabel()
        icon_label.setAlignment(QtCore.Qt.AlignCenter)
        pixmap = QtGui.QPixmap(":images/handInFilm.svg").scaled(80,80)
        icon_label.setPixmap(pixmap)

        f = QtCore.QFile(":VERSION.txt")
        f.open(QtCore.QIODevice.ReadOnly)
        VERSION = decode_byte_array(f.readLine().trimmed())
        
        f.close()

        CONTRIBUTERS = ""
        f = QtCore.QFile(":CONTRIBUTERS.txt")
        f.open(QtCore.QIODevice.ReadOnly)
        while True:
            contributer = f.readLine().trimmed()
            if not contributer:
                break
            contributer_text = decode_byte_array(contributer).replace(
                "<", "&lt;").replace(">", "&gt;")
            CONTRIBUTERS += "<li>%s</li>"% contributer_text
        f.close()

        TRANSLATORS = "%s<br />%s" %(
        _("Not yet translated, please visit"),
        '''<a href = 'https://translations.launchpad.net/auteur'>
        https://translations.launchpad.net/auteur</a>''')
        f = QtCore.QFile(":TRANSLATORS.txt")
        f.open(QtCore.QIODevice.ReadOnly)
        while True:
            translator = f.readLine().trimmed()
            if not translator:
                break
            translator_text = decode_byte_array(translator).replace(
                "<", "&lt;").replace(">", "&gt;")
            TRANSLATORS += "<li>%s</li>"% (translator_text)
            f.close()
        
        message = '''<h1>%s</h1>%s %s<br /><a href='%s'>%s</a><hr />'''% (
        _('The Auteur Video Editor'),
        _('Version'), VERSION,
        "http://auteur-editor.info", "http://auteur-editor.info")
        
        message2 = '''<h3>%s</h3><ul>%s</ul><hr />
        <h3>%s</h3><ul>%s</ul>'''% (
        _('Contributers'),CONTRIBUTERS,
        _('Translations'),TRANSLATORS)
        
        label1 = QtGui.QLabel()
        label1.setAlignment(QtCore.Qt.AlignCenter)
        label1.setWordWrap(True)
        label1.setText(message)
        
        label2 = QtGui.QLabel()
        label2.setText(message2)
        
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)        
        
        button_box.accepted.connect(self.accept)
        
        layout = QtGui.QVBoxLayout(self)
        #layout.setSpacing(3)
        layout.addWidget(icon_label)
        layout.addWidget(label1)
        layout.addWidget(label2)
        layout.addWidget(button_box)
        
        self.setMinimumSize(400,150)

        
        for label in (label1, label2):
            label.linkActivated.connect(self.open_link)


    def sizeHint(self):
        return QtCore.QSize(400,200)
        

    def open_link(self, link):
        webbrowser.open(link)
        
        
def _test(): 
    import gettext, os, sys
    gettext.install("auteur")
    
    sys.path.insert(0, os.path.abspath("../../"))
    from lib_auteur.components import q_resources
    
    app = QtGui.QApplication([])
    dl = AboutAuteurDialog()
    dl.exec_()
    
                   
if __name__ == "__main__":
    
    _test()
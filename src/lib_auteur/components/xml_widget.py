#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import re
from PyQt4 import QtCore, QtGui

try:
    from PyQt4 import Qsci
    BASECLASS = Qsci.QsciScintilla
except ImportError as e:
    print ("Qsci module not available - your XML won't be decorated")
    print (e)
    BASECLASS = QtGui.QTextEdit
except Exception as e:
    print ("Qsci module not available - your XML won't be decorated")
    print (e)
    BASECLASS = QtGui.QTextEdit

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))

from lib_auteur.components import ComponentWidget

class DataWidget(ComponentWidget):
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Data"))
        self.icon = QtGui.QIcon(":images/data.png")
        icon = QtGui.QIcon.fromTheme("document-open")
        self.load_button = QtGui.QPushButton(icon, _("Load Workbench"))
        
        icon = QtGui.QIcon.fromTheme("document-save")
        self.save_button = QtGui.QPushButton(icon, _("Save Workbench"))
        
        self.xml_widget = XMLWidget(self)
        
        layout = QtGui.QGridLayout(self)
        layout.addWidget(self.xml_widget,0,0,4,1)
        layout.addWidget(self.load_button,0,1)
        layout.addWidget(self.save_button,1,1)
        layout.addWidget(self.xml_widget.apply_button,3,1)        
        

class XMLWidget(BASECLASS):
    def __init__(self, parent=None):
        BASECLASS.__init__(self, parent)
        
        try:
            self.setLexer(Qsci.QsciLexerXML())
            self.is_qsci = True
        except AttributeError:
            self.is_qsci = False
            pass
        
        self._data_received = False
                
        self.model = None
        icon = QtGui.QIcon.fromTheme("document-send")
        self.apply_button = QtGui.QPushButton(icon, _("Apply Changes"))
        self.apply_button.setEnabled(False)

        message = (
        "This widget displays the raw form of your project data.\n\n"
        "You can also edit this manually.")

        self.setText(message)
        self.setToolTip(message)
        
        self.connect_signals()
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
        self.model.attach_view(self)
        
    def update_data(self):
        '''
        this is called from the data_model itself, when something happens.
        '''
        self.setText(self.model.xml_widget_text)
        
    def connect_signals(self):
        if self.is_qsci:
            self.modificationChanged.connect(self.modified)
        else:
            self.document().modificationChanged.connect(self.modified)
            
        self.apply_button.clicked.connect(self.apply)

    def modified(self, modded):
        self.apply_button.setEnabled(modded)
        
    def apply(self):
        try:
            self.model.load_string(self.current_text)
            QtGui.QMessageBox.information(self, _("Success"), 
                _("Changes Applied"))
        except IOError as e:
            QtGui.QMessageBox.warning(self, _("whoops"),
                "%s<hr />%s"% (_("Your changes are not acceptable"), e))
        self.apply_button.setEnabled(False)
        
    @property
    def current_text(self):
        if self.is_qsci:
            return str(self.text())
        else:
            return str(self.document().toPlainText())
        
    def setText(self, text):
        '''
        overwrite this so that I can disable the apply button when generated 
        code is loaded in
        '''
        BASECLASS.setText(self, text)
        self.apply_button.setEnabled(False)
    

def _test_geek():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = DataWidget()
        
    obj.sub_window.show()
    app.exec_()
    
    
if __name__ == "__main__":
    _test_geek()

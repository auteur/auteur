#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, sys

if __name__ == "__main__":
    sys.path.insert(0, os.path.abspath("../../"))
    
from PyQt4 import QtCore, QtGui

from lib_auteur.components.mplayer_core import Player
from lib_auteur.components import ComponentWidget

import tempfile, re, subprocess

class ScreenWidget(ComponentWidget):
    '''
    a custom widget which resizes the child player
    '''
    def __init__(self, mplayer_opts, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Screen"))
        self.screen = QtGui.QLabel(self)
        self.screen.setAlignment(QtCore.Qt.AlignCenter)
        self.screen.setStyleSheet("background:black;color:white")
        self.show_message1()

        self._Xscreen = QtGui.QX11EmbedWidget(self.screen)
        self._Xscreen.embedInto(self.screen.winId())
        self._mplayer = None
        
        self.mplayer_opts = mplayer_opts
        
        layout = QtGui.QVBoxLayout(self)
        layout.setMargin(0)
        layout.addWidget(self.screen)
        self.controls = None
        self.destroyed.connect(self._on_destroy)
        self.icon = QtGui.QIcon(":images/preview.png")
        
        self.is_playing_a_source = False
        
    def set_native(self, val):
        '''
        TODO
        '''
        if val == True:
            print ("use native mplayer")
        else:
            print ("use my player")
        
    def show_message1(self):
        self.screen.setText("%s<hr width='60%%'/>%s"%(
            _("No Source Video(s) Selected"),
            _("From the menu choose File - Get Source Video(s)")))
        
    def show_message2(self):
        self.screen.setText(
        _("Review a source, clip, or preview your timeline"))

    def __del__(self):
        self._on_destroy()
    
    def _on_destroy(self, *args):
        self.player.quit()
        
    def hideEvent(self, event=None):
        self.player.quit()

    def sizeHint(self):
        return QtCore.QSize(600,400)
        
    def resizeEvent(self, event=None):
        self._Xscreen.resize(self.screen.size())
        
    @property
    def player(self):
        if self._mplayer == None:
            self._mplayer = Player(self.mplayer_opts)
        return self._mplayer
        
    def set_controls(self, controls):
        '''
        controls is my video_controls widget
        '''
        self.controls = controls
        self.slider_pos = 0
        
        try:
            self.player.set_label(self.controls.label)
        except AttributeError as e:
            print("unable to set label", e)

        try:
            self.connect(self.player.signaller, QtCore.SIGNAL("completed"), 
                self.controls.reset)
            self.connect(self.player.signaller, QtCore.SIGNAL("paused"), 
                self.controls.paused)
        except AttributeError as e:
            print("unable to connect pause and complete signals", e)
        
        try:
            self.player.set_progress_indicator(self.controls.slider)
        except AttributeError as e:
            print("unable to set slider", e)
        
        try:
            self.controls.action_seekback.triggered.connect(self.backward1)
        except AttributeError as e:
            print("unable to add seekback",e)

        try:
            self.controls.play_pause.triggered.connect(self.play)
        except AttributeError as e:
            print("unable to add play_pause",e)
        
        try:
            self.controls.action_stop.triggered.connect(self.stop)
        except AttributeError as e:
            print("unable to add stop",e)

        try:
            self.controls.action_seekforward.triggered.connect(self.forward1)
        except AttributeError as e:
            print("unable to add seek forward",e)

        try:
            self.controls.slider.sliderPressed.connect(self.slider_down) 
            self.controls.slider.sliderMoved.connect(self.slider_moved)
            self.controls.slider.sliderReleased.connect(self.slider_up)
        except AttributeError as e:
            print("unable to connect slider",e)
        
        try:
            self.controls.action_split.triggered.connect(self.get_split_point)
        except AttributeError as e:
            print("unable to add split clip",e)
                
        self.connect(self.controls, QtCore.SIGNAL("speed_set"), 
            self.speed_set)
            
        self.layout().addWidget(self.controls)
        
    def quit(self):
        self.player.quit()
        
    def stop(self):
        self.player.quit()        
        
    def play(self):
        self.command("pause")
    
    def forward1(self):
        if self.player.monitor.is_paused:
            self.command("frame_step")        
        else:
            self.command("pausing_keep_force seek 5")

    def backward1(self):
        self.command("pausing_keep_force seek -5 2")

    def start_player(self, additional_args = [], source_review = True):
        #print ("Xscreen id = ", self._Xscreen.winId())
        self.show_widget(True)
        self.player.start(self._Xscreen.winId(), additional_args, source_review)
        if self.controls:
            self.controls.enable()
        
    def speed_set(self, i):
        self.command("pausing_keep_force speed_set %.02f"% i)
    
    def slider_down(self):
        print("SLIDER DOWN")
        self.command("pause")
        
    def slider_moved(self, i):
        
        self.slider_pos = i * self.controls.slider.OFFSET_VAL
        
    def slider_up(self):
        print("SLIDER/UP")
        self.command("pausing_keep_force seek %d 1"% self.slider_pos)
        self.command("pause")
        
    def get_split_point(self):
        self.command("pausing_keep_force get_time_pos")
        
    def command(self, command):
        '''
        send command to the underlying player
        see 
        http://www.mplayerhq.hu/DOCS/tech/slave.txt
        '''
        print("command: '%s'"% command) 
        if not self.player.is_alive():
            if self.player.source_file != "" and command.startswith("pause"):
                self.start_player()
            else:
                return
        retval = self.player._command(command)       
        print("returned:", retval)
        return retval
    
    def load_video(self, filename):
        if self.player.is_alive():
            print("alive player")
            self.player.quit()
        
        self.player.set_source_file(filename)
        self.start_player()
          
    def load_source(self, source):
        if self.controls:
            self.controls.reset()
            self.controls.source_settings()
            self.controls.slider.set_source_info(source.filename, source.length)
        self.load_clip(source, True)
        
    def load_clip(self, clip, clip_is_source=False):
        '''
        a "clip" is a custom data type of auteur
        this procedure is connected to the clip widget
        '''
        if clip is None:
            self.player.quit()
            self.show_message2()
            self.is_playing_a_source = False 
        elif clip.media_type == clip.IMAGE_MEDIA:
            self.player.quit()
            self.screen.setPixmap(clip.image.scaled(self.screen.size()))
        else:
            self.is_playing_a_source = clip_is_source          
            if self.player.is_alive():
                print("alive player")
                self.player.quit()
            
            additional_args = []
            if clip_is_source:
                additional_args.extend(["-loop", "0"])
            else:
                if self.controls:
                    self.controls.clip_settings()
                    
                if clip.start:
                    additional_args.extend([ "-ss", clip.start_format])
                if clip.end:
                    additional_args.extend(["-endpos", clip.end_format])
            
            if clip_is_source:
                self.player.set_source_file(clip.filename)
            else:
                additional_args.extend([clip.filename])
            
            self.start_player(additional_args, clip_is_source)
    
    def play_preview(self, model):
        self.player.quit()
        self.is_playing_a_source = False          
        self.controls.preview_settings()
        self.resizeEvent()
        self.show_widget(True)
        self.player.start(  self._Xscreen.winId(), 
                            model.preview_commands, 
                            source_review=False)

class _TestDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self, None)

        self.video_path = ""
        self.source_file = ""
        if len(sys.argv) > 1:
            self.source_file = sys.argv[1]

        self.screen = ScreenWidget([])
        
        self.controls = VideoControls()
        self.screen.set_controls(self.controls)
        
        icon = QtGui.QIcon.fromTheme("document-open")
        open_but = QtGui.QPushButton(icon, "")
        open_but.setFocusPolicy(QtCore.Qt.NoFocus)
        
        test_command_frame = QtGui.QFrame()
        self.command_line_edit = QtGui.QLineEdit()
        command_exec_but = QtGui.QPushButton("exec")
        test_command_frame.setMaximumHeight(40)

        layout = QtGui.QHBoxLayout(test_command_frame)
        layout.setMargin(0)
        layout.addWidget(open_but)
        layout.addWidget(self.command_line_edit)
        layout.addWidget(command_exec_but)

        command_exec_but.clicked.connect(self.command_test)
                
        layout = QtGui.QVBoxLayout(self)
        layout.setSpacing(0)
        layout.addWidget(self.screen)
        layout.addWidget(self.controls)        
        
        layout.addWidget(test_command_frame)
        
        
        open_but.clicked.connect(self.load_video)

        QtCore.QTimer.singleShot(0, self.get_command_list)
        if self.source_file != "":
            QtCore.QTimer.singleShot(0, self.start_player)
        
    def sizeHint(self):
        return QtCore.QSize(300,300)

    def quit(self):
        self.player.quit()
        self.screen.show()
        
    def clear(self):
        self.pos_slider.setValue(0)
        self.play_but.setIcon(self.play_icon)
        self.player.quit()
        self.screen.show()

    def paused(self, i):
        '''
        called via a signal in the monitoring thread
        '''
        if i:
            self.play_but.setIcon(self.play_icon)
        else:
            self.play_but.setIcon(self.pause_icon)

       
    def load_video(self):
        filename = QtGui.QFileDialog.getOpenFileName(self,
        "select video sources", self.video_path, "video files (*.*)")
        if filename:
            filename = str(filename).replace(" ","\ ")
            self.video_path = os.path.dirname(filename)
            if self.screen.player.is_alive():
                self.screen.player.quit()
                self.screen.command("loadfile %s"% filename)       
            else:
                self.screen.player.set_source_file(filename)
                self.screen.start_player()

    def command_test(self):
        '''
        executes the command in the testline edit
        '''
        self.screen.command(str(self.command_line_edit.text()))
    
    def get_command_list(self):
        '''
        pull the command list from mplayer
        '''
        try:
            raw_input  # note - not called, but this is now input in python3
            PYTHON3 = False
            print ("using PYTHON2")
        except NameError:
            PYTHON3 = True
            print ("using PYTHON3 = good!")
            
        def byte_decode(command):
            if PYTHON3:
                return command.decode("ascii").strip()
            else:
                return command.strip()

        p = subprocess.Popen(["mplayer","-input","cmdlist"], 
            stdout=subprocess.PIPE)
        p.wait()
        self.valid_mplayer_commands = []

        command = p.stdout.readline()

        while command:
            self.valid_mplayer_commands.append(byte_decode(command))
                    
            command = p.stdout.readline()
            
        if self.valid_mplayer_commands == []:
            QtGui.QMessageBox.warning(self, "error", 
                "mplayer command list is empty - this WILL cause problems")

        self.command_line_edit.setCompleter(
            QtGui.QCompleter(self.valid_mplayer_commands))
        
    def exec_(self):
        self.show()
        #QtGui.QApplication.instance().processEvents()
        QtGui.QDialog.exec_(self)

def _test_main():
    global VideoControls
    from lib_auteur.components.video_controls import VideoControls
    dl = _TestDialog()
    dl.exec_()

if __name__ == '__main__':
    import gettext
    gettext.install("auteur")
    
    app = QtGui.QApplication([])
    _test_main()
    
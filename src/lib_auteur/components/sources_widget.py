#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_auteur.components import ComponentWidget
from lib_auteur.components.sources_item_model import SourcesItemModel
from lib_auteur.components.source_properties_dialog import SourcePropertiesDialog

class Dragger(QtCore.QObject):
    def mouseMoveEvent(self, event):
        self.start_drag(event)
        
    def start_drag(self, event):
        index = self.indexAt(event.pos())
        if not index.isValid():
            return       
        self.setCurrentIndex(index) 
        source = self.model().data(index, QtCore.Qt.UserRole)
        if not source.is_source:
            return
        self.parent().parent().emit(QtCore.SIGNAL("dragging clip"), True)
        
        if source.media_type == source.IMAGE_MEDIA:
            d_text = "DRAGGED|%s|0|5.0"% source.filename
        else:
            d_text = "DRAGGED|%s"% source.filename
            
        mimeData = QtCore.QMimeData()
        mimeData.setText(d_text)
        
        drag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)

        drag.setPixmap(source.thumbnail)

        drag.setHotSpot(QtCore.QPoint(
            source.thumbnail.width()/2, source.thumbnail.height()/2))
        
        result = drag.start(QtCore.Qt.CopyAction)
    
class DragListView(QtGui.QListView, Dragger):
    def __init__(self, parent):
        QtGui.QListView.__init__(self, parent)
        self.setDragEnabled(True)
    
class DragTreeView(QtGui.QTreeView, Dragger):
    def __init__(self, parent):
        QtGui.QTreeView.__init__(self, parent)
        self.setDragEnabled(True)
    
class SourcesWidget(ComponentWidget):
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Sources"))
        self.icon = QtGui.QIcon(":images/sources.png")
        self._no_of_sources = 0
        self.source_layout = None
        self.label = QtGui.QLabel(_("No sources Loaded"))
        self.label.setWordWrap(True)
        icon = QtGui.QIcon(":images/sources.png")
        self.add_button = QtGui.QPushButton(icon, _("Add Sources"))
        self.add_button.setObjectName("sources")

        self.stacker = QtGui.QStackedWidget(self)

        self.list_view = DragListView(self)
        self.list_view.setViewMode(self.list_view.IconMode)       
        self.list_view2 = DragListView(self) 
        self.tree_view = DragTreeView(self)
        
        self.stacker.addWidget(self.list_view)
        self.stacker.addWidget(self.list_view2)
        self.stacker.addWidget(self.tree_view)
        
        self.combobox = QtGui.QComboBox()
        self.combobox.addItems([_("View"), _("Icons"), _("List"), _("Tree")])
        self.combobox.currentIndexChanged.connect(self.choose_view)
        
        layout = QtGui.QGridLayout(self)
        layout.setMargin(2)
        layout.addWidget(self.stacker,0,0,1,2)
        layout.addWidget(self.combobox, 1,1)
        layout.addWidget(self.label,1,0,)
        layout.addWidget(self.add_button,2,0,1,2)
        self.model = None
        self.setMinimumSize(100,200)
        
        self.setStyleSheet('''
            QPushButton#sources {background: #619597;}
            ''')  

        self.connect_signals()
                
        self.restore_settings()
        
    def connect_signals(self):
        for i in range(self.stacker.count()):
            view = self.stacker.widget(i)
            
            view.clicked.connect(self.source_clicked)
            view.doubleClicked.connect(self.preview_source)
        
    def sizeHint(self):
        return QtCore.QSize(100,300)
        
    def restore_settings(self):
        try:
            settings = QtCore.QSettings()
            chosen = settings.value("source_widget_choice")
            if chosen:
                try:
                    self.choose_view(int(chosen))
                except TypeError: #will happen with python2
                    self.choose_view(chosen.toInt()[0])
        except:
            print ("ERROR-non-critical applying settings for source widget")
            pass
            
    def choose_view(self, i):
        '''
        trigerred by the view choice combobox
        '''
        if i != 0:
            self.stacker.setCurrentIndex(i-1)
            settings = QtCore.QSettings()
            settings.setValue("source_widget_choice", i)
        self.combobox.setCurrentIndex(0)
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
        self.sources_item_model = SourcesItemModel(model)
        self.list_view.setModel(self.sources_item_model)
        self.list_view2.setModel(self.sources_item_model)
        self.tree_view.setModel(self.sources_item_model)
    
        self.model.attach_view(self)
        
    def preview_source(self, index):
        
        source = self.sources_item_model.data(index, QtCore.Qt.UserRole)
        if source.exists:
            self.emit(QtCore.SIGNAL("source chosen"), source)
        else:
            QtGui.QMessageBox.warning(self, _("ERROR"),
            _("The File Cannot be found"))
    
    def source_clicked(self, index):
        source = self.sources_item_model.data(index, QtCore.Qt.UserRole)   
        options = [
            "%s - %s"% (_("Review Source"), source.short_name), 
            _("Properties"),
            _("Remove Source from project")
            ]
        pos = self.sender().visualRect(index).topLeft()
        pos = self.mapToGlobal(pos)
        
        menu = QtGui.QMenu(self)

        for option in options:
            menu.addAction(option)
        result = menu.exec_(pos)
        if not result:
            return
        if result.text() == options[0]:
            self.preview_source(index)
        elif result.text() == options[1]:
            dl = SourcePropertiesDialog(source, self)
            dl.exec_()
        else:
            QtGui.QMessageBox.information(self, "whoops", "TODO")    
    
    def update_data(self):
            
        n = self.model.number_of_sources
        force_visibility = n > self._no_of_sources
        
        self._no_of_sources = n
        message = _("source") if n == 1 else _("sources")
        self.label.setText("<b>%s</b> %s"% (n, message))
        
        missing_files = []
        for source in self.model.sources:
            if not source.exists:
                missing_files.append(source.filename)
 
        if missing_files != []:
            missing = ""
            for missing_file in missing_files:
                missing += "<li>%s</li>"% missing_file
            
            QtGui.QMessageBox.warning(self, _("ERROR"),
            "%s<hr /><ul>%s</ul>"% (_("The following sources are missing!"),
            missing))
   
        self.sources_item_model.setupModelData()
        
        #if we have new sources.. force this window to appear and be raised.
        if force_visibility:
            self.show_widget(force_visibility)

def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = SourcesWidget() 
    
    from lib_auteur.components import DataModel
    
    model = DataModel() 
    test_proj = "../test_project/test_project.xml"
    model.load(test_proj)
    
    obj.set_model(model)
    obj.update_data()
    
    obj.sub_window.show()
    app.exec_()    

if __name__ == "__main__":
    _test_code()

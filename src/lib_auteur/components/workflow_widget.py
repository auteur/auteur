#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import re
from PyQt4 import QtCore, QtGui, QtSvg

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))

from lib_auteur.components import ComponentWidget


class Step0Widget(QtGui.QWidget):
    def __init__(self, workflow_widget):
        QtGui.QWidget.__init__(self, workflow_widget)
        
        svg_widg = QtSvg.QSvgWidget()
        svg_widg.load(":images/wizard.svg")
        
        label = QtGui.QLabel()
        label.setText("<body><h4>%s</h4><i>%s<br />%s</i></body>"% (
            _("1st time using Auteur?"),
            _("This wizard will guide you through the process"),
            _("Then you can hide it forever if you wish!")))
        
        label.setWordWrap(True)
        label.setAlignment(QtCore.Qt.AlignCenter)

        layout = QtGui.QVBoxLayout(self) 
        layout.addWidget(svg_widg)   
        layout.addWidget(label)


class Step1Widget(QtGui.QWidget):
    def __init__(self, workflow_widget):
        QtGui.QWidget.__init__(self, workflow_widget)
        self.workflow_widget = workflow_widget

        svg_widg = QtSvg.QSvgWidget()
        svg_widg.load(":images/handInFilm.svg")
                
        label = QtGui.QLabel()
        label.setText("<body>%s<br/><i>%s</i></body>"% (
            _("To get started, please select some video sources."),
            _("Note - these will not be altered in any way by auteur")))
        
        label.setWordWrap(True)
        label.setAlignment(QtCore.Qt.AlignCenter)

        icon = QtGui.QIcon(":images/sources.png")
        sources_button = QtGui.QPushButton(icon, 
            _("Get Source Videos"))
        sources_button.setFixedHeight(80)
        sources_button.setObjectName("sources")

        layout = QtGui.QVBoxLayout(self)  
        layout.addWidget(svg_widg)          
        layout.addWidget(label)
        layout.addWidget(sources_button)
    
        sources_button.clicked.connect(self.emit_)
    
    def emit_(self):
        self.workflow_widget.emit(QtCore.SIGNAL("load sources"))

class Step2Widget(QtGui.QWidget):
    def __init__(self, workflow_widget):
        QtGui.QWidget.__init__(self, workflow_widget)
        
        message = '''
<body><img align="left" src = "qrc:/images/clips.png" />
%s, %s<br />%s<br /><br clear="all"/><hr />
%s<ol><li>%s</li><li>%s %s</li></body>'''% (
_("Now you have source video"),
_("select the parts you wish to use"),
_("These parts are referred to as 'clips'"),
_("Clips can be obtained in 2 ways either:"),
_("Drag an entire source to the clips bucket or timeline"),
_("Review the source and break it into pieces with the pencil button."),
_("Then drag those pieces."))

        
        label = QtGui.QTextBrowser()
        label.setText(message)
        
        layout = QtGui.QVBoxLayout(self)    
        layout.addWidget(label)

class Step3Widget(QtGui.QWidget):
    def __init__(self, workflow_widget):
        QtGui.QWidget.__init__(self, workflow_widget)
        
        message = '''
<body><h4>%s</h4>%s<hr/>%s<ul><li>%s</li><li>%s</li><li>%s</li></ul><b>%s<br />%s</b></body>'''% (
_("You are now ready to render your video!"),
_("However, before you do, there are some optional steps"),
_("you MAY wish to:"),
_("alter the order of your clips - right click on a 'clip' for options"),
_("adjust the timings of your clips (as above)"),
_("Review the video (note- may differ slightly due to differences in the way mplayer and mencoder handle the timestamps)"),
_("BUT PLEASE - 1st Save your project."),
_("Choose 'save workbench' from the application menu."))
        
        
        label = QtGui.QTextBrowser()
        label.setText(message)
        
        layout = QtGui.QVBoxLayout(self)    
        layout.addWidget(label)
        
class Step4Widget(QtGui.QWidget):
    def __init__(self, workflow_widget):
        self.workflow_widget = workflow_widget
        QtGui.QWidget.__init__(self, workflow_widget)
        message = "<body><h1>%s</h1><i>%s %s %s</i></body>"%(
        _("Render your video"),
        _("Note - the render is performed in a subprocess"),
        _("meaning you can continue to edit your videos with auteur"),
        _("as it proceeds, or exit this part of the application."))
        
        label = QtGui.QLabel(message)
        label.setWordWrap(True)
        
        icon = QtGui.QIcon(":images/folder_video.png")        
        render_button = QtGui.QPushButton(icon, _("render the edited video"))
        render_button.setObjectName("render")
        
        layout = QtGui.QVBoxLayout(self)    
        layout.addWidget(label)
        layout.addWidget(render_button)
        
        render_button.clicked.connect(self.emit_)
        
    def emit_(self):
        self.workflow_widget.emit(QtCore.SIGNAL("render"))


class WorkflowWidget(ComponentWidget):
    def __init__(self, model, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("WorkFlow Wizard"))
        self.icon = QtGui.QIcon(":images/wizard.png")

        self.model = model

        self.stack = QtGui.QStackedWidget()
        
        self.stack.addWidget(Step0Widget(self))
        self.stack.addWidget(Step1Widget(self))
        self.stack.addWidget(Step2Widget(self))
        self.stack.addWidget(Step3Widget(self))
        self.stack.addWidget(Step4Widget(self))
        
        self.back_but = QtGui.QPushButton(_("Back"))
        self.next_but = QtGui.QPushButton(_("Let's Start!"))
        self.back_but.clicked.connect(self.back)
        self.next_but.clicked.connect(self.skip)
            
        layout = QtGui.QGridLayout(self)
        layout.addWidget(self.stack,0,0,1,2)
        layout.addWidget(self.back_but,1,0)
        layout.addWidget(self.next_but,1,1)
        
        self.setStyleSheet('''
            QWidget {background: white;}
            QPushButton#sources {background: #619597;}
            QPushButton#review {background: #599496;}
            QPushButton#sequence {background: #519496;}
            QPushButton#preview {background: #4a9496;}
            QPushButton#render {background: #429496;}
            ''')        
        
        self.new_project()

    def new_project(self):        
        self.view_sheet(0)
            
    def update_data(self):
        '''
        called whent the model changes
        '''
        n = self.model.number_of_sources 
        if n == 0:
            self.new_project()
            return
        n = self.model.number_of_clips
        if n == 0:
            self.view_sheet(2)
        else:
            self.view_sheet(3)
        
        
    def view_sheet(self, i):
        if i != 0:
            self.next_but.setText(_("Next"))
        self.back_but.setVisible(i != 0)
        self.next_but.setVisible(i != self.stack.count()-1)
        
        self.stack.setCurrentIndex(i)
        
        
    def back(self):
        i = self.stack.currentIndex()
        i = 0 if i==0 else i-1
        self.view_sheet(i)
    
    def skip(self):
        i = self.stack.currentIndex()
        max_ = self.stack.count()
        
        i =  (max_-1) if (i> max_-1) else i+1
        self.view_sheet(i)
        
def _test():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    from lib_auteur.components import DataModel    
    model = DataModel() 
    
    obj = WorkflowWidget(model)
    
    obj.sub_window.show()
    app.exec_()
    
    
if __name__ == "__main__":
    from lib_auteur.components import q_resources
    
    _test()

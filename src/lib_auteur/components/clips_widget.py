#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_auteur.components.clip_label import ClipLabel
from lib_auteur.components import ClipsComponentWidget

class ClipsWidget(ClipsComponentWidget):
    def __init__(self, parent=None):
        ClipsComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Clips"))
        self.icon = QtGui.QIcon(":images/clips.png")

        self.frame = QtGui.QFrame()
        self.frame.setObjectName("dropzone")
        self.clip_layout = QtGui.QHBoxLayout(self.frame)
        self.clip_layout.addStretch(0)

        self.scroll_area = QtGui.QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.frame)
        
        self.main_layout = QtGui.QGridLayout(self)
        self.orientation = QtCore.Qt.Horizontal
        self.add_clip_layout()
        self.model = None
        self.setAcceptDrops(True)
        self.set_bg()
        
    def resizeEvent(self, event=None):
        if self.width() < self.height():
            if self.orientation == QtCore.Qt.Horizontal:
                self.orientation = QtCore.Qt.Vertical
                self.add_clip_layout()
        else:
            if self.orientation == QtCore.Qt.Vertical:
                self.orientation = QtCore.Qt.Horizontal
                self.add_clip_layout()
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
        self.model.attach_view(self)
        
    def add_clip_layout(self):
        if self.orientation == QtCore.Qt.Horizontal:
            self.scroll_area.setVerticalScrollBarPolicy(
                QtCore.Qt.ScrollBarAlwaysOff)
            self.scroll_area.setHorizontalScrollBarPolicy(
                QtCore.Qt.ScrollBarAsNeeded)
        
            self.main_layout.addWidget(self.scroll_area,0,0,3,1)
            self.main_layout.addWidget(self.length_label,0,1)
            self.main_layout.addWidget(self.preview_button,1,1)
            self.main_layout.addWidget(self.render_button,2,1)
            self.clip_layout.setDirection(QtGui.QBoxLayout.LeftToRight)

        else:
            self.scroll_area.setVerticalScrollBarPolicy(
                QtCore.Qt.ScrollBarAsNeeded)
            self.scroll_area.setHorizontalScrollBarPolicy(
                QtCore.Qt.ScrollBarAlwaysOff)
        
            self.main_layout.addWidget(self.scroll_area,0,0)
            self.main_layout.addWidget(self.length_label,1,0)
            self.main_layout.addWidget(self.preview_button,2,0)
            self.main_layout.addWidget(self.render_button,3,0)
            self.clip_layout.setDirection(QtGui.QBoxLayout.TopToBottom)
            
    @property
    def clips(self):
        return self.model.clips(timeline_only=False)
    
    def add_clip(self, clip):
        label = ClipLabel()
        label.clip = clip
        label.setToolTip(clip.details)
        label.setPixmap(clip.thumbnail)
        self.clip_layout.insertWidget(self.clip_layout.count()-1,label)
    
        self.connect(label, QtCore.SIGNAL("clicked"), self.clip_clicked)
        self.connect(label, QtCore.SIGNAL("right clicked"), 
            self.clip_right_clicked)
        
    def deselect_cliplabels(self, label):
        '''
        deselects all but the named label
        '''    
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg != label:
                widg.is_selected = False
                widg.update()
    
    def select_clip_from_id(self, id):
        '''
        selects the clip with given id
        '''    
        for i in range(self.clip_layout.count()-1):
            label = self.clip_layout.itemAt(i).widget()
            if label.clip.id == id:
                label.select(emit_click = False)
                self.deselect_cliplabels(label)
                break
                
    def clip_clicked(self, clip=None):
        label = self.sender()
        self.deselect_cliplabels(label)
        self.emit(QtCore.SIGNAL("clip selected"), label.clip)
        
    def clip_right_clicked(self, pos):
        label = self.sender()
        self.deselect_cliplabels(label)
        
        #show_menu is a method of the base class
        self.show_menu(label.clip, pos)
    
    @property
    def selected_clip(self):            
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg.is_selected:
                return widg.clip
            
    def update_data(self):
        while self.clip_layout.count()>1:
            item = self.clip_layout.takeAt(0)
            item.widget().deleteLater()        
        
        for clip in self.clips:
            self.add_clip(clip)    
        
        if self.model.total_clips == 0:
            message = _("No Clips Added!")
        else:        
            message = "%s %s %s"% (_("output length"), 
                self.model.duration, _("seconds"))

        self.length_label.setText(message)
    
    def flash_drop(self):
        self.set_bg(True)
        QtCore.QTimer.singleShot(500, self.set_bg)
            
    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            self.set_bg(True)
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasText():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        self.set_bg()

    def dropEvent(self, event):
        data = event.mimeData()
        if data.hasText():
            data = data.text().split("|")
            dragged = str(data[0]).startswith("DRAGGED")
            if not dragged:
                self.set_bg()
                event.ignore()
                return
            filename = str(data[1])
            try:
                start = float(data[2])
                end = float(data[3])
                self.model.add_clip(filename, start, end)
            except IndexError:
                self.model.use_full_source_as_clip(filename)       
        self.set_bg()
        event.accept()

    def set_bg(self, active=False):
        if active:
            val = "QFrame#dropzone {background:yellow;}"
        else:
            val = "QFrame#dropzone {}"
        self.setStyleSheet(val)

        
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = ClipsWidget() 
    
    from lib_auteur.components import DataModel
    
    model = DataModel() 
    model.load("../test_project/test_project.xml")
    obj.set_model(model)
    obj.update_data()
    obj.sub_window.show()

    app.exec_()    

if __name__ == "__main__":
    _test_code()

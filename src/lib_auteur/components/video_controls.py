#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division
 
import os, sys

from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    sys.path.insert(0, os.path.abspath("../../"))

from lib_auteur.components.clip_grabbing_slider import ClipGrabbingSlider


class VideoControls(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        
        self.slider = ClipGrabbingSlider()

        self.label = QtGui.QLabel("monitor")
        self.label.setFont(QtGui.QFont("courier"))        
        self.label.setMinimumWidth(120) 
        self.label.setFixedHeight(
            QtGui.QApplication.instance().fontMetrics().height())       
        self.toolbar = QtGui.QToolBar(self)
        self.toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly) #TextUnderIcon)

        self.icon_mark = QtGui.QIcon(":images/pencil.png")
        self.icon_split = QtGui.QIcon.fromTheme("edit-cut")
        
        self.insert_player_buttons()
        
        self.layout = QtGui.QVBoxLayout(self)
        self.layout.setMargin(0)
        self.layout.setSpacing(0)
        self.layout.addWidget(self.slider) #,0,0)
        self.layout.addWidget(self.label)#,0,1)
        self.layout.addWidget(self.toolbar)#,1,0,1,2)
        
        self.setMaximumHeight(120)
        
    def sizeHint(self):
        return QtCore.QSize(400,70)    
    
    def minimumSizeHint(self):
        return QtCore.QSize(300,100)
    
    def insert_player_buttons(self):
        
        self.video_controls = []
        
        icon = QtGui.QIcon.fromTheme("media-seek-backward")
        self.action_seekback = QtGui.QAction(icon, _("reverse"), self)
        self.action_seekback.setToolTip(_("skip backwards 5 seconds"))
        self.toolbar.addAction(self.action_seekback)
        self.video_controls.append(self.action_seekback)
        
        self.play_icon = QtGui.QIcon.fromTheme("media-playback-start")
        self.pause_icon = QtGui.QIcon.fromTheme("media-playback-pause")
        self.play_pause = QtGui.QAction(self.play_icon, _("Play"), self)
        self.toolbar.addAction(self.play_pause)
        self.video_controls.append(self.play_pause)
        
        icon = QtGui.QIcon.fromTheme("media-playback-stop")
        self.action_stop = QtGui.QAction(icon, _("Stop"), self)
        self.toolbar.addAction(self.action_stop)
        self.video_controls.append(self.action_stop)
                
        icon = QtGui.QIcon.fromTheme("media-seek-forward")
        self.action_seekforward = QtGui.QAction(icon, _("forwards"), self)
        self.action_seekforward.setToolTip(_("skip forward 5 seconds"))
        self.toolbar.addAction(self.action_seekforward)
        self.video_controls.append(self.action_seekforward)
                
        self.toolbar.addSeparator()
        self.action_split = QtGui.QAction(
            self.icon_split, _("Split Clip"), self)
        self.action_split.setToolTip(
            _("Split the clip at the current time position"))  
        self.toolbar.addAction(self.action_split)
        self.video_controls.append(self.action_split)

        self.toolbar.addSeparator()
        frame = QtGui.QFrame()
        self.speed_label = QtGui.QLabel()
        self.speed_label.setFont(QtGui.QFont("courier"))  
        self.speed_set(100)      
        self.speed_dial = QtGui.QDial()
        self.speed_dial.setFixedSize(40,40)
        self.speed_dial.setRange(1,10000)
        self.speed_dial.setValue(100)
        self.speed_dial.setNotchesVisible(True)
        self.speed_dial.setNotchTarget(100)

        self.speed_dial.valueChanged.connect(self.speed_set)

        layout = QtGui.QHBoxLayout(frame)
        layout.setMargin(0)
        layout.addWidget(self.speed_label)
        layout.addWidget(self.speed_dial)
        self.toolbar.addWidget(frame)
        self.video_controls.append(frame)
        

        self.enable(False)
        
    def reset(self):
        self.paused(True)
        self.speed_dial.setValue(100)
        self.slider.setValue(0)
        
    def paused(self, i):
        '''
        called via a signal in the monitoring thread
        '''
        if i:
            self.play_pause.setIcon(self.play_icon)
            self.play_pause.setText(_("Play"))
        else:
            self.play_pause.setIcon(self.pause_icon)
            self.play_pause.setText(_("Pause"))
        
        
    def enable(self, enable=True):
        for action in self.video_controls:
            action.setEnabled(enable)
            
    def speed_set(self, i):
        i = i/100
        
        speed = "%.2fx"% i
        
        self.speed_label.setText("Speed%s"% speed.rjust(7))
        self.emit(QtCore.SIGNAL("speed_set"), i)
            
    def disable(self):
        self.enable(False)
        
    def source_settings(self):
        self.slider.clear()
        self.enable()
        self.slider.show()
        self.action_split.setIcon(self.icon_mark)
        self.action_split.setText("timestamp")
        self.action_split.setToolTip(
            _("Mark source at the current time position")) 
        
    def clip_settings(self):
        self.enable()
        self.slider.hide()
        self.action_split.setIcon(self.icon_split)
        self.action_split.setText("split_clip")
        self.action_split.setToolTip(
            _("Split the clip at the current time position")) 
    
    def preview_settings(self):
        self.enable()
        self.slider.hide()
        self.clip_settings()
        
def _test_controls():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    vc = VideoControls()        
    vc.enable()
    
    vc.show()

    app.exec_()    

if __name__ == "__main__":
    _test_controls()

#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtGui, QtCore

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))

try:
    raw_input
    PYTHON3 = False
except NameError:
    PYTHON3 = True 

HORIZONTAL_HEADERS = ("file", "type", "full filepath/id", "Length")

class TreeItem(object):
    '''
    a python object used to return row/column data, and keep note of
    it's parents and/or children
    '''
    def __init__(self, data_item, header, parentItem):
        self.data_item = data_item
        self.parentItem = parentItem
        self.header = header
        self.childItems = []

    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return len(HORIZONTAL_HEADERS)

    def data(self, column, role = QtCore.Qt.DisplayRole):
        if self.data_item is None:
            if column == 0:
                return self.header
        elif role == QtCore.Qt.DisplayRole:
            if column == 0:
                if self.data_item.is_source:
                    return self.data_item.short_name
                else:
                    return "clip"
            if column == 1:
                if self.data_item.media_type == self.data_item.VIDEO_MEDIA:
                    return _("VIDEO")
                elif self.data_item.media_type == self.data_item.IMAGE_MEDIA:
                    return _("IMAGE")
                elif self.data_item.media_type == self.data_item.AUDIO_MEDIA:
                    return _("AUDIO")
                else:
                    return _("Unknown")
                
            if column == 2:
                return self.data_item.id
            if column == 3:
                return self.data_item.length
        
        if PYTHON3:
            return None
        else:
            return QtCore.QVariant()
        
    def parent(self):
        return self.parentItem

    def row(self):
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0

class SourcesItemModel(QtCore.QAbstractItemModel):
    '''
    a model to which can be attached to a variety of views, but shows most 
    info when used with a QTreeView
    '''
    def __init__(self, model, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.model = model
        self.setupModelData()
    
    def columnCount(self, parent=None):
        if parent and parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return len(HORIZONTAL_HEADERS)

    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()

        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            return item.data(index.column())
        if index.column() == 0 and role == QtCore.Qt.DecorationRole:
            return item.data_item.icon
        if role == QtCore.Qt.UserRole:
            if item:
                return item.data_item

        if PYTHON3:
            return None
        else:
            return QtCore.QVariant()

    def headerData(self, column, orientation, role):
        if (orientation == QtCore.Qt.Horizontal and
        role == QtCore.Qt.DisplayRole):
            try:
                return HORIZONTAL_HEADERS[column]
            except IndexError:
                pass

        if PYTHON3:
            return None
        else:
            return QtCore.QVariant()

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        if not childItem:
            return QtCore.QModelIndex()

        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            p_Item = self.rootItem
        else:
            p_Item = parent.internalPointer()
        return p_Item.childCount()

    def setupModelData(self):
        self.beginResetModel()
        self.rootItem = TreeItem(None, "ALL", None)
        self.parents = {"" : self.rootItem}
        i = 0
        for source in self.model.sources:
            i += 1
            newparent = TreeItem(source, "source %s"% i , self.rootItem)
            self.rootItem.appendChild(newparent)
            self.parents[source.filename] = newparent
        for clip in self.model.clips():
            parent = self.parents.get(clip.filename, "")
            parent.appendChild(TreeItem(clip, "", parent))
        self.endResetModel()

class _TestDialog(QtGui.QDialog):
    def __init__(self, model, parent=None):
        super(_TestDialog, self).__init__(parent)
        
        self.combo_box = QtGui.QComboBox(self)
        self.combo_box.setModel(model)
        
        self.list_view = QtGui.QListView(self)
        self.list_view.setModel(model)
        self.list_view.setAlternatingRowColors(True)
        
        self.table_view = QtGui.QTableView(self)
        self.table_view.setModel(model)
        self.table_view.setAlternatingRowColors(True)

        self.tree_view = QtGui.QTreeView(self)
        self.tree_view.setModel(model)
        self.tree_view.setAlternatingRowColors(True)

        layout = QtGui.QVBoxLayout(self)
        layout.addWidget(self.combo_box)        
        layout.addWidget(self.list_view)
        layout.addWidget(self.table_view)
        layout.addWidget(self.tree_view)

        
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    from lib_auteur.components import DataModel
    
    model = DataModel() 
    test_proj = "../test_project/test_project.xml"
    model.load(test_proj)
    
    sources_item_model = SourcesItemModel(model)
    
    dl = _TestDialog(sources_item_model)
    dl.exec_()

if __name__ == "__main__":
    _test_code()


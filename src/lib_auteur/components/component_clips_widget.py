#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

'''
An extension for ComponentWidget.

this ensures that "clip views" currently (clipswidget and timeline)
are kept synchronised.

for example.. keeps a note of which clip is "playing"
'''

from PyQt4 import QtGui, QtCore

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))

from lib_auteur.components.component_base_widget import ComponentWidget
from lib_auteur.components.source_properties_dialog import SourcePropertiesWidget

class ClipAdjusterDialog(QtGui.QDialog):
    def __init__(self, clip, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(_("Clip Adjuster"))
        
        label = QtGui.QLabel()
        image = clip.image.scaled(200,200)
        label.setPixmap(image)
        
        start_label = QtGui.QLabel(_("Start"))
        start_label.setAlignment(QtCore.Qt.AlignCenter)
        self.start_spinbox = QtGui.QDoubleSpinBox(self)
        self.start_spinbox.setValue(clip.start_float)
        self.start_spinbox.setRange(0, clip.source_length)
        
        end_label = QtGui.QLabel(_("End"))        
        end_label.setAlignment(QtCore.Qt.AlignCenter)
        self.end_spinbox = QtGui.QDoubleSpinBox(self)
        self.end_spinbox.setValue(clip.end_float)
        self.end_spinbox.setRange(0, clip.source_length)
        
        button_box = QtGui.QDialogButtonBox(self)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        button_box.setStandardButtons(button_box.Apply|button_box.Cancel)
         
        layout = QtGui.QGridLayout(self)
        layout.addWidget(label,0,0,1,2)
        layout.addWidget(start_label,1,0)
        layout.addWidget(self.start_spinbox,2,0)
        layout.addWidget(end_label,1,1)        
        layout.addWidget(self.end_spinbox,2,1)
        layout.addWidget(button_box,3,0,1,2)
        
        button_box.clicked.connect(self.accept)
        button_box.rejected.connect(self.reject)
        

class ClipsComponentWidget(ComponentWidget):
    '''
    An extension for ComponentWidget.

    this ensures that "clip views" currently (clipswidget and timeline)
    are kept synchronised.
    
    '''
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        
        icon = QtGui.QIcon.fromTheme("media-playback-start")
        self.preview_button = QtGui.QPushButton(_("Preview"))
        self.preview_button.setIcon(icon)
        
        self.render_button = QtGui.QPushButton(_("Render"))
        icon = QtGui.QIcon.fromTheme("media-record")
        self.render_button.setIcon(icon)
        self.length_label = QtGui.QLabel()
        self.length_label.setText(_("No Clips Loaded"))
        self.length_label.setWordWrap(True)
        
        self.preview_button.clicked.connect(self.preview)
        self.render_button.clicked.connect(self.render)

        self.currently_selected_id = None
    
    def sizeHint(self):
        return QtCore.QSize(400,100)
        
    def show_menu(self, clip, pos):
        if clip.in_timeline:
            tl_mess = _("remove from timeline")
        else:
            tl_mess = _("add to timeline")            
    
        options = [
            _("Load clip into editor"),
            _("edit start and end positions"),
            tl_mess, 
            _("Advance clip (move left)"),
            _("Retreat clip (move right)"),            
            _("Delete Clip from project")
            ]
        menu = QtGui.QMenu(self)
        for option in options:
            menu.addAction(option)
        result = menu.exec_(pos)
        if not result:
            return
        if result.text() == options[0]:
            self.clip_clicked(clip)
        elif result.text() == options[1]:
           self.adjust_clip_times(clip)
        elif result.text() == options[2]:
           self.model.toggle_clip_in_timeline(clip)
        elif result.text() == options[3]:
           self.model.advance_clip(clip)
        elif result.text() == options[4]:
           self.model.retreat_clip(clip)
        elif result.text() == options[5]:
            self.emit(QtCore.SIGNAL("delete clip"), clip)
    
    def preview(self):
        self.emit(QtCore.SIGNAL("preview"))
        
    def render(self):
        self.emit(QtCore.SIGNAL("render"))
    
    def clip_clicked(self, clip):
        self.emit(QtCore.SIGNAL("clip selected"),clip)
        
    def adjust_clip_times(self, clip):
        dl = ClipAdjusterDialog(clip, self)
        if dl.exec_():
            print ("new start %s"% dl.start_spinbox.value())
            print ("new end %s"% dl.end_spinbox.value())
            
            start = dl.start_spinbox.value()
            end = dl.end_spinbox.value()
            if end <= start:
                QtGui.QMessageBox.warning(self, 
                _("error"), _("bad time sequence"))
            else:
                self.model.adjust_clip_times(clip, start, end)
        
    def receive_time_data(self, data):
        '''
        model updates this every second!
        '''
        clip_id = data[0] 
        if clip_id != self.currently_selected_id:
            self.currently_selected_id = clip_id
            self.select_clip_from_id(clip_id)
        
    
        
if __name__ == "__main__":
    
    app = QtGui.QApplication([])
    from lib_auteur.components import DataModel
    
    import gettext
    gettext.install("auteur")

    model = DataModel() 
    test_proj = "../test_project/test_project.xml"
    model.load(test_proj)
    dl = ClipAdjusterDialog(model.clips()[0])
    dl.exec_()

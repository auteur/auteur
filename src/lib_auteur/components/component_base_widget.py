#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtGui, QtCore

class SubWindow(QtGui.QMainWindow):
    def __init__(self, widget, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.widget = widget
        if self.widget.icon != None:
            self.setWindowIcon(self.widget.icon)
        else:
            self.setWindowIcon(QtGui.QIcon(":images/handInFilm.svg"))
        self.setWindowTitle(self.windowTitle())
        self.setObjectName("%s_sub_window"% widget.windowTitle())
        self.setMinimumSize(120, 80)
        self.setCentralWidget(widget)
        
    def hideEvent(self, event):
        self.save_settings()
        QtGui.QMainWindow.hideEvent(self, event)
        
    def closeEvent(self, event):
        if not QtGui.QApplication.instance().closingDown():
            self.widget.action.setChecked(False)
        QtGui.QMainWindow.closeEvent(self, event)
        
    def showEvent(self, event):
        QtGui.QMainWindow.showEvent(self, event)
        geom =  self.geom
        if geom:
            self.restoreGeometry(self.geom)
        self.widget.action.setChecked(True)
        
    def save_settings(self):
        settings = QtCore.QSettings()        
        settings.setValue("%s_geom"% self.objectName(), self.saveGeometry())
    
    @property
    def geom(self):
        settings = QtCore.QSettings()
        geom = settings.value("%s_geom"%self.objectName())
        try: #python2 
            geom = geom.toByteArray()
        except AttributeError:
            pass
        return geom
        
class ComponentWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setWindowTitle("please set a window title")
        self._sub_window = None
        self._dock_widget = None
        self._show_action = None
        self.icon = None
    
    def objectName(self):
        return self.windowTitle()
    
    @property
    def action(self):
        '''
        overwrite ComponentWidget's action property.. 
        because screen widget is never docked
        '''
        if self._show_action is None:
            self._show_action = QtGui.QAction(self.windowTitle(), self)
            self._show_action.setCheckable(True)
            self._show_action.setChecked(True)            
            self._show_action.triggered.connect(self.show_widget)
            if self.icon != None:
                self._show_action.setIcon(self.icon)
        return self._show_action

    def sizeHint(self):
        return QtCore.QSize(400,100)

    def show_widget(self, val):
        if self.has_sub_window:
            self.sub_window.setVisible(val)
            self.sub_window.raise_()
            
        elif self.has_dock:
            self.dock_widget.setVisible(val)
            self.dock_widget.raise_()
        
        self.action.setChecked(val)
        
    @property
    def empty_dock(self):
        '''
        returns the dock widget, but only if it exists and is empty
        '''
        if self._dock_widget is None:
            return
        if self._dock_widget.widget() is None: 
            return self._dock_widget
    
    @property
    def dock_widget(self):
        '''
        A dockWidget wrapper for the ClipsWidget
        '''
        if self._dock_widget is None:
            self._dock_widget = QtGui.QDockWidget(self.parent())
            self._dock_widget.setWindowTitle(self.windowTitle())
            self._dock_widget.setWindowIcon(QtGui.QIcon())
            self._dock_widget.setObjectName("%s_dock"% self.windowTitle())
            self._dock_widget.setWidget(self)
        if self._dock_widget.widget() is None:
            self._dock_widget.setWidget(self)
            self.delete_sub_window()
        return self._dock_widget
    
    @property
    def sub_window(self):
        '''
        A sub_window wrapper for the ClipsWidget
        '''
        if self._sub_window is None:
            self._sub_window = SubWindow(self)
            self._sub_window.setWindowTitle(self.windowTitle())
            self._sub_window.setObjectName("%s_dialog"% self.windowTitle())
        return self._sub_window
    
    @property
    def has_dock(self):
        return self._dock_widget != None

    @property
    def has_sub_window(self):
        return self._sub_window != None
    
    def delete_sub_window(self):
        if self.has_sub_window:
            self._sub_window.hide()
            self._sub_window.deleteLater()
            self._sub_window = None

if __name__ == "__main__":
    print ("no useable test for this module, sorry")
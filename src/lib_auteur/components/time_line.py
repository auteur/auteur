#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division

import math, os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_auteur.components import ClipsComponentWidget

class TimeLine(QtGui.QLabel):
    def __init__(self, parent):
        QtGui.QLabel.__init__(self, parent)
        self.time_line_parent = parent
        self.clear()
        self.setMouseTracking(True)
        self.updating = False
        
        #some geometry constants
        self.STRIP_WIDTH = 12
        self.PADDING = 4  #space above and below the strip
    
    def clear(self):
        '''
        reset the timeline
        '''
        self._duration = 0
        self._position = 0
        self._marker_polygon = None
        self.clips = []
        self.marker_colour=QtGui.QColor("red")
        self.stretch()
        
    @property
    def duration(self):
        '''
        the value (in seconds) of the current data
        '''
        return self._duration
    
    @property
    def position(self):
        '''
        the position (in seconds) of the play position
        '''
        return self._position
    
    def set_position(self, val):
        '''
        set the time_pos of the marker. 
        '''
        if val != self._position:
            self._position = val
            self._marker_polygon = None
            self.update()
    
    @property
    def marker_polygon(self):
        if self._marker_polygon is None:
            
            p = self.position * self.stretch_val
            h = self.height()
            
            points = []
            points.append(
                QtCore.QPoint(  p-1, self.PADDING + self.STRIP_WIDTH))
            
            points.append(QtCore.QPoint(  p-6, 0))
            
            points.append(QtCore.QPoint(  p+6, 0))
                    
            points.append(
                QtCore.QPoint(  p+1, self.PADDING + self.STRIP_WIDTH))
                                    
            points.append(
                QtCore.QPoint(  p+1, h- self.PADDING - self.STRIP_WIDTH))

            points.append(QtCore.QPoint(  p+6, h-1))
            
            points.append(QtCore.QPoint(  p-6, h-1))
            
            points.append(
                QtCore.QPoint(  p-1, h- self.PADDING - self.STRIP_WIDTH))
                    
            self._marker_polygon = QtGui.QPolygon(points)
        
        return self._marker_polygon
    
    @property
    def marker_x_range(self):
        pos = int(self.position * self.stretch_val)
        return (pos-2, pos-1, pos, pos+1, pos+2)
        
    def set_duration(self, val):
        self._duration = val
        self.stretch()
            
    def stretch(self, val=0):
        
        self.stretch_val = math.exp(val/10)
        width = self.stretch_val * self.duration
        
        if self.width() == width:
            self.resizeEvent()
        else:
            self.setFixedWidth(width)
        self.update()
        
    def set_model(self, model):
        self.model = model

    def resizeEvent(self, event=None):
        self.updating = True
        left_x = 0
        for clip in self.clips:
            right_x = left_x + (clip.length * self.stretch_val)
            clip.tl_rect = QtCore.QRectF(   left_x,  
                                            15 , 
                                            right_x-left_x, 
                                            self.height()-31)
            left_x = right_x
            clip.mouse_over = False
        self._marker_polygon = None
        self.updating = False            
        
    def update_data(self, slider_val):
        self.updating = True #disable paint Event
        self.clips = self.model.clips()[:] #quick copy
        self.set_duration(self.model.duration)
        self.stretch(slider_val)
        self.updating = False
        self.update()
        
    def mouseMoveEvent(self, event):            
        self.updating = True #disable paint Event
        
        if event.pos().x() in self.marker_x_range:
            self.marker_colour=QtGui.QColor("blue")
            self.clear_clip_mousing()
        else:
            self.marker_colour=QtGui.QColor("red")
            pos = QtCore.QPointF(event.pos()) 
                       
            for clip in self.clips:
                try:
                    clip.mouse_over = clip.tl_rect.contains(pos) 
                except AttributeError:
                    clip.mouse_over = False
        self.updating = False
        self.update()
    
    def mousePressEvent(self, event):
        pos = QtCore.QPointF(event.pos())
        for clip in self.clips:
            if clip.tl_rect.contains(pos):
                if event.button() == 2:
                    self.clip_right_clicked(clip, 
                        self.mapToGlobal(event.pos()))
                else:
                    self.clip_clicked(clip)
                break
    
    def clear_clip_mousing(self):
        for clip in self.clips:
            clip.mouse_over = False           
        
        
    def leaveEvent(self, event):
        self.updating = True #disable paint Event
        self.marker_colour=QtGui.QColor("red")
        self.clear_clip_mousing()
        self.updating = False
        self.update()
    
    def clip_clicked(self, clip):
        print ("emitting")
        self.time_line_parent.emit(QtCore.SIGNAL("clip selected"), clip)
    
    def clip_right_clicked(self, clip, pos):
        self.time_line_parent.show_menu(clip, pos)
        
    def paintEvent(self, event):
        if self.updating:
            return
        painter = QtGui.QPainter(self)
    
        option = QtGui.QTextOption()
        option.setAlignment(QtCore.Qt.AlignCenter)

        ## DRAW BLACK LINES ABOVE AND BELOW

        painter.fillRect(QtCore.QRect(0, self.PADDING, self.width(), self.STRIP_WIDTH), 
            QtGui.QColor("black"))
            
        painter.fillRect(QtCore.QRect(
            0, self.height()-self.PADDING-self.STRIP_WIDTH, self.width(), self.STRIP_WIDTH), 
            QtGui.QColor("black"))

        painter.save()
        for clip in self.clips:
            if self.updating:
                #data (and therefore all geometries) undergoing update
                #so postpone this event.
                painter.restore()
                return
            if clip.mouse_over:
                colour = "yellow"
            else:
                colour = "#c8e5e6"
            painter.fillRect(clip.tl_rect, QtGui.QColor(colour)) 
            painter.drawRect(clip.tl_rect) 

            painter.setPen(QtGui.QColor("black"))
            font = painter.font()
            font.setBold(True)
            font.setPointSize(24)
            painter.setFont(font)
            id = clip.id.lstrip("0")
            painter.drawText(clip.tl_rect, id, option)

            painter.save()
            painter.translate(1,1)
            painter.setPen(QtGui.QColor("red"))
            painter.drawText(clip.tl_rect, id, option)
            painter.restore()
            
        painter.restore()    
        
        x = 2
        
        white = QtGui.QColor("white")
        block_size = self.stretch_val/0.25
        while x < self.width():
            painter.fillRect(QtCore.QRect(x,7,block_size,6), white)
            
            bottom_rect = QtCore.QRect(x,self.height()-13,block_size,6)
            painter.fillRect(bottom_rect, white)

            x += self.stretch_val*8
            
        ## draw the time position marker
        painter.setBrush(self.marker_colour)
        painter.setPen(self.marker_colour)
        
        painter.drawPolygon(self.marker_polygon)
            
class DropScrollArea(QtGui.QScrollArea):
    def __init__(self, parent=None):
        QtGui.QScrollArea.__init__(self, parent)
        self.setMinimumSize(200,100)
        self.setAcceptDrops(True)
        self.set_bg()
        
    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            self.set_bg(True)
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasText():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        self.set_bg()

    def dropEvent(self, event):
        data = event.mimeData()
        if data.hasText():
            data = data.text().split("|")
            dragged = str(data[0]).startswith("DRAGGED")
            if not dragged:
                self.set_bg()
                event.ignore()
                return
            filename = str(data[1])
            try:
                start = float(data[2])
                end = float(data[3])
                self.parent().model.add_clip(filename, start, end)
            except IndexError:
                self.parent().model.use_full_source_as_clip(filename)       
        self.set_bg()
        event.accept()

    def set_bg(self, active=False):
        if active:
            val = "QScrollArea {background:yellow;}"
        else:
            val = "QScrollArea {}"
        self.setStyleSheet(val)

class TimeLineWidget(ClipsComponentWidget):
    def __init__(self, parent=None):
        ClipsComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("TimeLine"))
        self.icon = QtGui.QIcon(":images/clock.png")
        
        self.slider = QtGui.QSlider()
        self.slider.setRange(-50,70) #values are used logarithmically
        self.slider.setValue(0)
        
        self.scroll_area = DropScrollArea(self)
        self.timeline = TimeLine(self)
        self.scroll_area.setWidget(self.timeline)
        self.scroll_area.setWidgetResizable(True)
        
        layout = QtGui.QGridLayout(self)
        
        layout.addWidget(self.slider,0,0,3,1)
        layout.addWidget(self.scroll_area,0,1,3,1)
        layout.addWidget(self.length_label,0,2)
        layout.addWidget(self.preview_button,1,2)
        layout.addWidget(self.render_button,2,2)
        
        self.slider.valueChanged.connect(self.timeline.stretch)
    
    def flash_drop(self):
        self.scroll_area.set_bg(True)
        QtCore.QTimer.singleShot(500, self.scroll_area.set_bg)
    
    def set_model(self, model):
        self.model = model
        self.timeline.set_model(model)
        model.attach_view(self)
        
    def update_data(self):
        self.timeline.update_data(self.slider.value())
        self.length_label.setText(
            "output length %d seconds"% self.timeline.duration)

    def receive_time_data(self, data):
        '''
        model updates this every second!
        '''
        time_pos = data[1]
        self.timeline.set_position(time_pos)
    
    
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = TimeLineWidget() 
    
    from lib_auteur.components import DataModel
    
    model = DataModel() 
    test_proj = "../test_project/test_project.xml"
    model.load(test_proj)
    
    obj.set_model(model)
    obj.update_data()
    obj.sub_window.show()

    app.exec_()    

if __name__ == "__main__":
    _test_code()

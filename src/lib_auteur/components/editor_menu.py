#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtCore, QtGui

class EditorMenu(QtCore.QObject):
    def __init__(self):
        '''
        parent should be a mainwindow
        '''
        self.menubar = self.menuBar()
        self._menu_insertpoint = None
        self.tool_bars = []
        
        tool_bar = QtGui.QToolBar()
        tool_bar.setObjectName("project-toolbar")
        tool_bar.setWindowTitle(_("Project"))
        self.tool_bars.append(tool_bar)
        
        tool_bar = QtGui.QToolBar()
        tool_bar.setObjectName("video-toolbar")
        tool_bar.setWindowTitle(_("Sources"))
        self.tool_bars.append(tool_bar)

        tool_bar = QtGui.QToolBar()
        tool_bar.setObjectName("data-toolbar")
        tool_bar.setWindowTitle(_("Data"))
        self.tool_bars.append(tool_bar)

        tool_bar = QtGui.QToolBar()
        tool_bar.setObjectName("ui-toolbar")
        tool_bar.setWindowTitle(_("Ui Options"))
        self.tool_bars.append(tool_bar)
            
        self.init_actions()
        self.init_menu()
        self.init_toolbars()
        self.added_actions = []
    
    def init_actions(self):
        
        ####          file menu                                            ####

        icon = QtGui.QIcon.fromTheme("application-exit")
        self.action_quit = QtGui.QAction(icon, _("Quit"), self)

        icon = QtGui.QIcon.fromTheme("document-new")
        self.action_new_workspace = QtGui.QAction(icon, _("New WorkBench"), 
            self)
        
        icon = QtGui.QIcon.fromTheme("document-open")
        self.action_load_workspace = QtGui.QAction(icon, _("Load WorkBench"), 
            self)

        icon = QtGui.QIcon.fromTheme("document-save")
        self.action_save_workspace = QtGui.QAction(icon, _("Save WorkBench"), 
            self)

        icon = QtGui.QIcon(":images/sources.png")
        self.action_get_sources = QtGui.QAction(icon, _("Get Source Video(s)"), 
            self)
        
        ####        edit menu                                              ####
        icon = QtGui.QIcon.fromTheme("edit-undo")
        self.action_undo = QtGui.QAction(icon, _("Undo Last Edit"), self)
        self.action_undo.setEnabled(False)
        
        icon = QtGui.QIcon.fromTheme("edit-redo")
        self.action_redo = QtGui.QAction(icon, _("Redo"), self)
        self.action_redo.setEnabled(False)

        ####         view menu                                             ####

        self.action_docked = QtGui.QAction(_("Docked Windows"), self)
        self.action_docked.setCheckable(True)
        self.action_docked.setChecked(True)
        self.action_multi = QtGui.QAction(_("Multiple Top Level Windows"),self)
        self.action_multi.setCheckable(True)
        
        icon = QtGui.QIcon.fromTheme("view-default")
        self.action_default_layout = QtGui.QAction(icon,
            "%s %s"% (_("Restore All Widgets"), "(F12)"), self)
        self.action_default_layout.setShortcut("f12")
        
        icon = QtGui.QIcon.fromTheme("view-fullscreen")
        self.action_fullscreen = QtGui.QAction(icon,
            "%s %s"% (_("FullScreen Mode"), "(F11)"), self)
        self.action_fullscreen.setCheckable(True)
        self.action_fullscreen.setShortcut("f11")

        ####         about menu                                            ####

        icon = QtGui.QIcon.fromTheme("help-about")
        self.action_about = QtGui.QAction(icon, _("About"), self)

        self.action_about_qt = QtGui.QAction(icon, _("About Qt"), self)

        self.action_license = QtGui.QAction(icon, _("License"), self)

        icon = QtGui.QIcon.fromTheme("help", QtGui.QIcon(":images/help.png"))
        self.action_help = QtGui.QAction(icon, _("Help"), self)
        
        self.action_docked.triggered.connect(self.toggle_ui_mode)
        self.action_multi.triggered.connect(self.toggle_ui_mode)
    
    def insert_toolbar_menu(self, menu):
        menu.setTitle(_("Toolbars"))
        self.menu_view.insertSeparator(self._menu_insertpoint)
        self.menu_view.insertMenu(self._menu_insertpoint, menu)
        self.menu_view.insertSeparator(self._menu_insertpoint)
    
    def insert_view_action(self, action):
        '''
        inserts actions for the popup_menu generated by the mainwindow
        '''
        self.added_actions.append(action)
        self.menu_view.insertAction(self._menu_insertpoint, action)
        self.tool_bars[3].addAction(action)
        
    def toggle_ui_mode(self):
        toggled = self.sender()
        if toggled is self.action_docked:
            self.action_multi.setChecked(not self.action_docked.isChecked())
        else:
            self.action_docked.setChecked(not self.action_multi.isChecked())
        
        self.emit(QtCore.SIGNAL("update ui"))
        
    def init_menu(self):
        ## menu
        self.menu_file = QtGui.QMenu(_("&File"), self)
        self.menubar.addMenu(self.menu_file)
        self.menu_file.addAction(self.action_new_workspace) 
        self.menu_file.addAction(self.action_load_workspace)
        self.menu_file.addAction(self.action_save_workspace)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.action_get_sources)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.action_quit)

        ## edit
        self.menu_edit = QtGui.QMenu(_("&Edit"), self)
        self.menubar.addMenu(self.menu_edit)

        self.menu_edit.addAction(self.action_undo)
        self.menu_edit.addAction(self.action_redo)

        ## view
        self.menu_view = QtGui.QMenu(_("&View"), self)
        self.menubar.addMenu(self.menu_view)

        self.menu_view.addAction(self.action_docked)
        self.menu_view.addAction(self.action_multi)
        
        self._menu_insertpoint = self.menu_view.addSeparator()
        
        self.menu_view.addSeparator()
        self.menu_view.addAction(self.action_default_layout)
        
        self.menu_view.addSeparator()        
        self.menu_view.addAction(self.action_fullscreen)

        ## help
        self.menu_help = QtGui.QMenu(_("&Help"), self)
        self.menubar.addMenu(self.menu_help)
        
        self.menu_help.addAction(self.action_about)
        self.menu_help.addAction(self.action_license)
        self.menu_help.addAction(self.action_about_qt)
        self.menu_help.addSeparator()
        self.menu_help.addAction(self.action_help)
    
    def init_toolbars(self):
        self.tool_bars[0].addAction(self.action_new_workspace) 
        self.tool_bars[0].addAction(self.action_load_workspace)
        self.tool_bars[0].addAction(self.action_save_workspace)
        
        self.tool_bars[1].addAction(self.action_get_sources)
        
        self.tool_bars[2].addAction(self.action_undo)
        self.tool_bars[2].addAction(self.action_redo)

class _TestClass(QtGui.QMainWindow, EditorMenu):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        EditorMenu.__init__(self)
    
        for toolbar in self.tool_bars:
            self.addToolBar(toolbar)
    
    def sizeHint(self):
        return QtCore.QSize(400,200)
    
def _test_menu():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    mw = _TestClass()
    
    mw.show()
    mw.insert_toolbar_menu(mw.createPopupMenu())
    
    
    app.exec_()    

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))
    from lib_auteur.components import q_resources
    _test_menu()

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

'''
my_core is a re-implementation of code from the python-mplayer code

which is 

Copyright (C) 2010  Darwin M. Bautista <djclue917@gmail.com>

why? The -quiet flag is unwanted for my editor.

I need to leverage more of mplayer's output, in a purely pyqt manner
'''
import os, re, subprocess, sys

from PyQt4 import QtGui, QtCore

try:
    raw_input
    PYTHON3 = False
    LINEENDS = bytes("\n\r")
    
except NameError:
    PYTHON3 = True 
    LINEENDS = bytes("\n\r", "ascii")
    
class MonitoringThread(QtCore.QThread):
    def __init__(self, signaller, process, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.process = process
        
        self.signaller = signaller
        self.label = None
        self.progress_indicator = None
        self.is_paused = True
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.check_progress)
        self.timer.start()
        self.reviewing_source = False
    
    def set_label(self, label):
        self.label = label
        
    def set_progress_indicator(self, pb):
        try:
            pb.setValue
        except AttributeError:
            raise AssertionError(
            "%s does not have a setValue Function"% pb +
            "your progress indicator will not work")
            
        self.progress_indicator = pb
        
    def set_review_mode(self, val):
        self.reviewing_source = val
        
    def check_progress(self):
        self.signaller.emit(QtCore.SIGNAL("paused"), self.is_paused)
         
    def run(self):
        '''
        we watch out for information returned on standard out,
        either as a result of a video playing, or by queries sent by the user
        
        as an example, when the user sends "get_time_position" into stdout
        ANS_TIME_POSITION=12.7 is returned.
        
        we watch for this and emit a signal via the supplied Qlabel
        '''
    
        if PYTHON3:
            VERBOSE = str(QtCore.QSettings().value("verbose")).lower() == "true"
        else:
            VERBOSE = QtCore.QSettings().value("verbose").toBool()
    
        self.last_known_timestamp = 0
        
        while True:
            out = ""
            char = self.process.stdout.read(1)
            while char not in LINEENDS:
                out += char.decode("ascii", "ignore")
                char = self.process.stdout.read(1)
            
            #get rid of ALL white space (different mplayers and shells?)
            out = out.strip()
             
            if out.startswith("=====  PAUSE  ====="):
                self.is_paused = True
                print ("PAUSED!")
                self.signaller.emit(QtCore.SIGNAL("paused"), self.is_paused)
                
            elif out.startswith("Playing "):
                filename = out[8:]
                print ("EMITTING NEW FILE", filename)
                self.signaller.emit(QtCore.SIGNAL("file changed"), filename)
                
            elif out.startswith("ANS_TIME_POSITION="):
                m = re.match("ANS_TIME_POSITION=(\d+\.\d+)", out)
                self.signaller.emit(QtCore.SIGNAL("split position"),
                    float(m.groups()[0]))
            
            elif out == 'Exiting... (End of file)':
                print ("EOF REACHED")
                break
            
            else:
                self.is_paused = False
                #'A:   2.1 V:   2.1 '
                
                ## let's pass the float value accross (ie. 10 updates a second)
                m = re.search(" V: *(\d+\.\d+) ", out)
                if m:
                    pos = float(m.groups()[0])                    
                    if pos != self.last_known_timestamp:
                        self.last_known_timestamp = pos
                        if self.progress_indicator is not None:
                            self.progress_indicator.setValue(
                                pos, pass_on = not self.reviewing_source)
            
                if self.label:
                    self.label.setText(out)
            
            ##debug
            if VERBOSE:
                print ("OUT:'%s'"% out)
            
        print("STDOUT FINISHED")
        if not self.progress_indicator:
            self.timer = None
            self.quit() # destroy this thread
        else:
            self.progress_indicator.setValue(0)
            self.quit()
            
class Player(object):    
    def __init__(self, args=[]):
        
        self.args = [
            'mplayer',
            '-ni',
            '-slave',
            '-input',
            'nodefault-bindings',
            '-noconfig', 
            'all']
        #'-idle', 
        self.args.extend(args)
        self._proc = None
        
        self.label = None
        self.signaller = QtCore.QObject()
        self.progress_indicator = None
        self.controls = None
        self._source_file = ""
        self.monitor = None
        
    def __del__(self):
        try:
            self.monitor.terminate()
        except AttributeError:
            pass
        self.quit()

    def __repr__(self):
        if self.is_alive():
            status = 'with pid = %d' % (self._proc.pid)
        else:
            status = 'not running'
        return '<%s.%s %s>' % (__name__, self.__class__.__name__, status)

    @property
    def source_file(self):
        return self._source_file
        
    def set_source_file(self, source_file):
        self._source_file = source_file
    
    def start(self, winId, additional_args=[], source_review=True):
        '''
        Start the MPlayer process.
        Returns None if MPlayer is already running.
        '''        
        if source_review and self.source_file == "":
            print("no sourcefile yet!")
            return
        if not self.is_alive():
            mplayer_args = self.args[:]
            mplayer_args.extend(["-wid", str(winId)] )
            mplayer_args.extend(additional_args)
            if source_review:
                mplayer_args.extend([self.source_file])        
                mplayer_args.extend(['-osdlevel', '3'])
            else:
                mplayer_args.extend(['-osdlevel', '1'])
                
            print(mplayer_args)
            self._proc = subprocess.Popen(mplayer_args, 
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, 
                stderr=subprocess.STDOUT)
                #close_fds=True)
                
        self.monitor = MonitoringThread(self.signaller, 
                                        self._proc, 
                                        self.signaller)
        self.monitor.set_review_mode(source_review)
        self.monitor.start()
        if self.label:
            self.monitor.set_label(self.label)
        if self.progress_indicator:
            self.monitor.set_progress_indicator(self.progress_indicator)
                        
    def set_label(self, label):
        assert type(label) == QtGui.QLabel, "label must be a QLabel"
        self.label = label
        
    def set_progress_indicator(self, progress_bar):
        assert progress_bar.setValue, (
            'ERROR mplayer_core set_progress_indicator' +
            '- widget must have a setValue function')
        
        self.progress_indicator = progress_bar
        
    def quit(self, retcode=0):
        """Stop the MPlayer process.

        Returns the exit status of MPlayer or None if not running.
        """
        if self.monitor is not None:
            self.monitor.terminate()
            self.monitor = None

        self.set_source_file("")

        if self.is_alive():
            self._stdout = None
            self._stderr = None
            self._command("quit")
            return self._proc.wait()
                
        
    def is_alive(self):
        """Check if MPlayer process is alive.

        Returns True if alive, else, returns False.
        """
        if self._proc is None:
            return False
        else:
            return (self._proc.poll() is None)
    
    if PYTHON3:        
        def _command(self, command):
            '''
            Send a command to MPlayer.
            Valid MPlayer commands are documented in:
            http://www.mplayerhq.hu/DOCS/tech/slave.txt
            '''
            if self.is_alive() and command:
                self._proc.stdin.write(bytes(command+"\n", "ascii"))
                self._proc.stdin.flush()
    else:
        def _command(self, command):
            '''
            Send a command to MPlayer.
            Valid MPlayer commands are documented in:
            http://www.mplayerhq.hu/DOCS/tech/slave.txt
            '''
            if self.is_alive() and command:
                self._proc.stdin.write(bytes(command+"\n"))
                self._proc.stdin.flush()
        

class _TestDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self, None)

        self.video_path = ""
        self.source_file = ""
        if len(sys.argv) > 1:
            self.source_file = sys.argv[1]

        self.screen = QtGui.QLabel("no video loaded")
        self.screen.setAlignment(QtCore.Qt.AlignCenter)
        self.screen.setStyleSheet("background:black;color:white")

        self.label = QtGui.QLabel("details")
        self.label.setMaximumHeight(self.fontMetrics().height())
        
        icon = QtGui.QIcon.fromTheme("document-open")
        open_but = QtGui.QPushButton(icon, "", self)
        open_but.setFocusPolicy(QtCore.Qt.NoFocus)
        
        
        layout = QtGui.QGridLayout(self)
        layout.setSpacing(0)
        layout.addWidget(self.screen,0,0,1,2)        
        layout.addWidget(open_but,1,0)
        layout.addWidget(self.label,1,1)        
        
        self.player = Player()
            
        self.player.set_label(self.label)
        
        self.rejected.connect(self.quit)
        
        open_but.clicked.connect(self.load_video)
        
        if self.source_file != "":
            QtCore.QTimer.singleShot(0, self.start_player)
        
    def sizeHint(self):
        return QtCore.QSize(300,300)

    def quit(self):
        self.player.quit()
        
    def clear(self):
        self.player.quit()
        self.screen.show()

    def start_player(self):
        self.player.set_source_file(self.source_file)
        self.player.start(self.screen.winId(), source_review=True)
                
    def load_video(self):
        filename = QtGui.QFileDialog.getOpenFileName(self,
        "select video sources", self.video_path, "video files (*.*)")
        if filename:
            pyname = str(filename)
            self.video_path = os.path.dirname(pyname)
            if self.player.is_alive():
                self.player.quit()
            self.source_file = pyname
            self.start_player()
    
    def exec_(self):
        self.show()
        QtGui.QDialog.exec_(self)

def _test_main():
    dl = _TestDialog()
    dl.exec_()

if __name__ == '__main__':

    app = QtGui.QApplication([])
    _test_main()
    
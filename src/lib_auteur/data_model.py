#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division

import mimetypes, os, re, shlex, subprocess, tempfile

from PyQt4 import QtCore, QtGui
from xml.dom import minidom

VALIDATE = True
try:
    from PyQt4.QtXmlPatterns import (   QXmlSchemaValidator, 
                                        QXmlSchema)
except ImportError:
    VALIDATE = False

class Source(object):
    '''
    a custom data type to describe a media source
    '''
    VIDEO_MEDIA = 0
    IMAGE_MEDIA = 1
    AUDIO_MEDIA = 2
    
    def __init__(self):
        self.filename = ""
        self.is_source = True
        self._image = None
        self._thumbnail = None
        self._icon = None
        self._screenshot_pos = "1.0"
        self._id = ""
        self._properties = None
        self.media_type = self.VIDEO_MEDIA
        
        #properties accessed during painting
        self.mouse_over = False
        self.tl_rect = QtCore.QRectF()
        
    @property
    def short_name(self):
        return os.path.basename(self.filename)
        
    @property
    def exists(self):
        '''
        is the file available?
        '''
        return os.path.exists(self.filename)
            
    @property    
    def id(self):
        '''
        A UNIQUE 4 digit code for the clip
        '''
        return self.filename
    
    @property
    def length(self):
        return float(self.properties.get("ID_LENGTH", 1))
    
    @property
    def screenshot_pos(self):
        if self._screenshot_pos is None:
            try:
                self._screenshot_pos = "%.1f"% float(self.start)
            except ValueError:
                self._screenshot_pos = "0.0"
        return self._screenshot_pos
    
    @property
    def icon(self):
        if self._icon is None:
            self._icon = QtGui.QIcon(self.thumbnail)
        return self._icon
    
    @property
    def thumbnail(self):
        if self._thumbnail is None:
            self._thumbnail = self.image.scaled(80,80)
        return self._thumbnail
    
    @property
    def image(self):
        if not self.exists:
            return QtGui.QPixmap(":images/unknown.png")
        if self._image is None:
            key = "%s%s"% (self.filename, self.screenshot_pos)
            try:
                self._image = KNOWN_IMAGES[key]
                return self._image
            except KeyError:
                pass
            
            try:
                temp = tempfile.gettempdir()
                src1 = os.path.join(temp, "00000001.jpg")
                #use mplayer to grab a screenshot (ss)
                commands = ["mplayer", "-nolirc", "-ss", 
                    self.screenshot_pos, self.filename,
                    "-frames", "1", "-nosound", "-vo", 
                    "jpeg:outdir=%s"% temp]
                p = subprocess.Popen(commands, stdout=subprocess.PIPE)
                p.wait()
                #print ("trying src1", key)
                pixmap = QtGui.QPixmap(src1)
            except Exception as e:
                print("UNABLE TO GET IMAGE FOR %s"% self.id, e)
                pixmap = QtGui.QPixmap()
            
            if pixmap.isNull():
                pixmap = QtGui.QPixmap(":images/unknown.png")
            
            KNOWN_IMAGES[key] = pixmap
            self._image = pixmap
            
        return self._image
    
    @property
    def properties(self):
        if not self.exists:
            self._properties is None
            return {}
            
        if self._properties is None:
            key = "%s%s"% (self.filename, self.id)
            try:
                self._properties = KNOWN_PROPERTIES[key]
                return self._properties
            except KeyError:
                pass
            self._properties = {}
            try:
                #use mplayer to get clip properties
                p = subprocess.Popen(["mplayer", "-nolirc", "-vo", "null", 
                "-ao", "null", "-frames", "0", "-identify", self.filename], 
                stdout=subprocess.PIPE)
                
                comm = p.communicate()[0]
                for encoding in ("ascii", "utf8", "latin-1"):
                    try:
                        result = comm.decode(encoding)
                        break
                    except UnicodeDecodeError:
                        result = ""
                        pass

                for val in (
                    "ID_FILENAME",
                    "ID_DEMUXER",
                    "ID_LENGTH", 
                    "ID_VIDEO_FORMAT", 
                    "ID_VIDEO_BITRATE",
                    "ID_VIDEO_WIDTH",
                    "ID_VIDEO_HEIGHT",
                    "ID_VIDEO_FPS",
                    "ID_VIDEO_ASPECT",
                    "ID_AUDIO_FORMAT",
                    "ID_AUDIO_BITRATE",
                    "ID_AUDIO_RATE",
                    "ID_AUDIO_NCH",
                    "ID_SEEKABLE",
                    "ID_CHAPTERS",                   
                ):
                
                    match=re.search("%s=(.*)\n"% val, result)
                    if match:
                        self._properties[val] = match.groups()[0]                

            except Exception as e:
                print("ERROR! unable to get clip properties!", e)
            
            KNOWN_PROPERTIES[key] = self._properties
    
        return self._properties
    
    @property
    def width(self):
        return int(self.properties.get("ID_VIDEO_WIDTH",0))

    @property
    def height(self):
        return int(self.properties.get("ID_VIDEO_HEIGHT",0))
    
    @property
    def framerate(self):
        return float(self.properties.get("ID_VIDEO_FPS",0))
    
class ImageSource(Source):
    '''
    A custom data object representing a still picture
    '''
    def __init__(self):
        Source.__init__(self)
        self.filename = ""
        self.duration = 0
        self._id = ""
        self.media_type = self.IMAGE_MEDIA
    
    def __repr__(self):
        return ("Image - %s"% self.filename)
    
    @property
    def image(self):
        if not self.exists:
            return QtGui.QPixmap(":images/unknown.png")
        if self._image is None:
            key = self.filename
            try:
                self._image = KNOWN_IMAGES[key]
                return self._image
            except KeyError:
                pass
            pixmap = QtGui.QPixmap(self.filename)
                
            if pixmap.isNull():
                pixmap = QtGui.QPixmap(":images/unknown.png")
            
            KNOWN_IMAGES[key] = pixmap
            self._image = pixmap
            
        return self._image
    
    @property
    def width(self):
        return self.image.width()

    @property
    def height(self):
        return self.image.height()
    
    @property
    def length(self):
        '''
        maximum time an image can be displayed is limited by the length
        of my silent audio, however, 5 minutes seems practical
        '''
        return 300
    
class Clip(Source):
    '''
    a custom data type to describe a clip
    '''
    def __init__(self):
        Source.__init__(self)
        
        self.is_source = False 
        self._start = ""
        self._end = ""
        self._screenshot_pos = None
        self.in_timeline = True
        self._time_line_offset = 0
        
    def __repr__(self):
       return "CLIP %s"% self.id
 
    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return self.id == other.id
    
    @property
    def id(self):
        return self._id
    
    @property
    def start(self):
        '''
        the start position of the clip within the source video file
        default is ""   (ie. the start)
        '''
        return self._start
    
    @property
    def start_float(self):
        try:
            return float(self.start)
        except ValueError:
            return 0
        
    @property
    def end(self):
        '''
        the end position of the clip within the source video file
        default is ""   (ie. the end)
        '''
        return self._end
    
    @property
    def end_float(self):
        try:
            return float(self.end)
        except ValueError:
            return self.source_length
    
    @property
    def start_format(self):
        '''
        the start in format "00:00:00.0"
        '''
        if self.start == "":
            return
        start = float(self.start)
        assert start >= 0, (
            "FATAL ERROR: clip id (%s)has a negative start time"% self.id)
        hours = start//3600
        start = start - hours*3600
        mins = start//60
        secs = start-mins*60
        tenths = (secs - int(secs))*10
        return "%02d:%02d:%02d.%d"% (hours, mins, int(secs), tenths)
        
    @property
    def end_format(self):
        '''
        the end in format "00:00:00.0" (for --endpos)

        NOTE - in mplayer/mencoder --endpos is dependendent on the value of 
        --ss
        '''
        if self.end == "":
            return
        end = float(self.end)
        if self.start != "":
            end = end - self.start_float
        assert end > 0, (
            "FATAL ERROR: clip id (%s) is of negative length"% self.id)
        hours = end//3600
        end = end - hours*3600
        mins = end//60
        secs = end-mins*60
        tenths = (secs - int(secs))*10
        return "%02d:%02d:%02d.%d"% (hours, mins, int(secs), tenths)
        
    @property
    def details(self):        
        return "Clip ID %s<br />source '%s'<br />Start %s<br />End %s"% (
                self.id,
                self.filename, 
                self.start,
                self.end)
                
    @property
    def is_partial(self):
        '''
        does the clip equate to the whole of the source?
        ''' 
        return not (self.start == "" and self.end == "")
    
    @property
    def source_length(self):
        return float(self.properties.get("ID_LENGTH", 0))

    @property
    def length(self):
        if self.start != "":
            start = float(self.start)
        else:
            start = 0
        if self.end != "":
            return float(self.end) - start
        
        return self.source_length - start
    
    def set_time_line_offset(self, offset):
        self._time_line_offset = offset
    
    @property
    def time_line_offset(self):
        '''
        example - if this is a 3 second, but 48.1 seconds of other clips 
        precede it.. then the value is 48.1
        '''
        if self.in_timeline:
            return self._time_line_offset #-float(self.start)
        else:
            return 0
       
    @property
    def preview_command_list(self):
        commands = []
        commands += [self.filename]
        if self.start != "":
            commands += ["-ss", self.start_format]
        if self.end != "":
            commands += ["-endpos", self.end_format]
        return commands
    
class ImageClip(ImageSource):
    '''
    a custom data type to describe a clip derived from an image
    '''
    def __init__(self):
        ImageSource.__init__(self)
        self.media_type = self.IMAGE_MEDIA
        self.is_source = False 
        self._screenshot_pos = None
        self.in_timeline = True
        self._time_line_offset = 0
        self._preview_video = None
        self._end = "5"
        
    def __repr__(self):
       return "image CLIP %s"% self.id
 
    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return self.id == other.id
    
    @property
    def id(self):
        return self._id
    
    @property
    def start(self):
        '''
        the start position of the clip within the source video file
        default is ""   (ie. the start)
        '''
        return ""
    
    @property
    def start_float(self):
        return 0
        
    @property
    def end(self):
        '''
        the end position of the clip within the source video file
        default is ""   (ie. the end)
        '''
        return self._end
    
    @property
    def end_float(self):
        try:
            return float(self.end)
        except ValueError:
            return self.source_length
    
    @property
    def details(self):        
        return '''Image Clip ID %s<br />source '%s'<br />
    Playing for %s seconds'''% (
                self.id,
                self.filename, 
                self.length)
                
    @property
    def is_partial(self):
        '''
        does the clip equate to the whole of the source?
        ''' 
        return False
    
    @property
    def source_length(self):
        return 300.0

    @property
    def length(self):
        if self.start != "":
            start = float(self.start)
        else:
            start = 0
        if self.end != "":
            return float(self.end) - start
        
        return self.source_length - start
    
    def set_time_line_offset(self, offset):
        self._time_line_offset = offset
    
    @property
    def time_line_offset(self):
        '''
        example - if this is a 3 second, but 48.1 seconds of other clips 
        precede it.. then the value is 48.1
        '''
        if self.in_timeline:
            return self._time_line_offset #-float(self.start)
        else:
            return 0
        
    @property
    def preview_command_list(self):
        return ["mf://%s"% self.filename, 
                "-mf", "fps=%.1f"% (1/self.length)]
     
class DataModel(object):
    def __init__(self, doc=None):
        if doc is None:
            doc = self.new_doc()
        self.doc = doc
        
        self.history = [doc.toxml()]
        self.redo_list = []
        self.last_saved_pos = 0
        
        self.view_widgets = []
        self.time_view_widgets = []
        self.signaller = QtCore.QObject()
        self._clip_id = 0
        self._clips = {}
        self._duration = None
        self.currently_playing_file = None
        self.currently_playing_clip = None
        self.media_types = {}   # a key-value store of media types 
                                #for each file in source_files
        
    def new_doc(self):
        doc = minidom.Document()
        top_level = doc.createElement("auteur")
        doc.appendChild(top_level)
    
        return doc

    def new_workspace(self):
        self.doc = self.new_doc()
        self._duration = 0
        self.get_max_clip_id()
        self.history = [self.xml]
        self.last_saved_pos = 0
        self.update_views()        
    
    def load(self, filename):
        try:
            doc = minidom.parse(filename)
        except Exception as e:
            raise IOError ("PARSING ERROR %s"% e)
        if not self.validate(doc.toxml()):
            raise IOError ("file is not a valid auteur XML file, sorry!")
        
        self.doc = doc
        self.remove_relative_paths(filename)
        self.get_max_clip_id()
        self.history = [self.xml]
        self.last_saved_pos = 0
        self._clips = {}
        self.update_views()
        
    def load_string(self, xml):
        '''
        called when user edits the xml by hand
        '''
        try:
            doc = minidom.parseString(xml)
        except Exception as e:
            raise IOError ("PARSING ERROR %s"% e)
        if not self.validate(doc.toxml()):
            raise IOError ("file is not a valid auteur XML file, sorry!")
        self.doc = doc
        self.update_history()
        
    def validate(self, xml):
        if not VALIDATE:
            print ("WARNING - unable to check validity of XML")
            return True
        
        print ("ATTEMPTING VALIDATION WITH QtXmlSchema classes! (experimental)")

        f = QtCore.QFile(":schemas/auteur_v02.xsd")
        f.open(QtCore.QIODevice.ReadOnly)
        schema = QXmlSchema()
        schema.load(f)

        validator = QXmlSchemaValidator(schema)
        result = validator.validate(xml)
        
        return result
    
    def get_max_clip_id(self):
        self._clip_id = 0
        for node in self.doc.getElementsByTagName("clip"):
            id = node.getAttribute("clip-id")
            if id:
                i = int(id)
                if i > self._clip_id:
                    self._clip_id = i
    
    def attach_view(self, view_widget):
        '''
        attach a view to this model.
        this means that whenever the model is altered,
        the widget's update_data function will be called, allowing it
        to pull whatever data it needs
        
        if the widget being attached has a function receive_player_info
        then it will be passed a tuple (clip, time_pos) every second.
        '''
        assert view_widget.update_data
        try:
            view_widget.receive_time_data
            self.time_view_widgets.append(view_widget)
        except AttributeError:
            pass
        self.view_widgets.append(view_widget)
    
    @property
    def undo_available(self):
        return len(self.history) > 1
    
    @property
    def redo_available(self):
        return len(self.redo_list) > 0
        
    @property
    def next_clip_id(self):
        '''
        a counter to ensure each clip is uniquely id'd
        '''
        self._clip_id += 1
        return "%04d"% self._clip_id
    
    @property
    def is_dirty(self):
        try:
            return self.history[self.last_saved_pos] != self.xml
        except IndexError:
            return True
        
    @property
    def has_changed(self):
        return self.history[-1] != self.xml
    
    @property
    def tempfile(self):
        '''
        save a temporary file and return it's path
        '''
        temp = tempfile.mktemp(suffix=".xml")
        self.save_file(temp)
        return temp
    
    def save_file(self, filename):
        '''
        use relative paths when saving so that workbenches can be shared
        '''
        try:
            savedir = os.path.dirname(filename)
            commondir = os.path.commonprefix(self.source_files)
            
            while not os.path.isdir(commondir):
                commondir = commondir[:-1]
                if commondir == "":
                    break # shouldn't happen
            
            copy_doc = self.doc.cloneNode(True)
            source_nodes = copy_doc.getElementsByTagName("source")
            for source_node in source_nodes:
                abspath = source_node.getAttribute("location")
                rel_path = os.path.relpath(abspath, commondir)
                source_node.setAttribute("location", rel_path)
            
            rel_node = copy_doc.createElement("relative-path")
            
            root = os.path.relpath(commondir, savedir)
            rel_node.setAttribute("root", root)
            
            copy_doc.documentElement.insertBefore(rel_node, source_nodes[0])
            
            f = open(filename, "w")
            f.write(self._beautified_xml(copy_doc.toxml()))
            f.close()
            self.last_saved_pos = len(self.history)-1
            return (True, "")

        except Exception as e:
            return (False, e)
        
    
    def update_history(self):
        if len (self.history) > 50:
            self.history = self.history[-50:]
        if self.has_changed:
            self.history.append(self.xml)
            self._duration = None
            self.update_views()
            
    @property
    def has_sources(self):
        return not self.source_files == []
    
    def update_views(self):
        for widget in self.view_widgets:
            widget.update_data()
            
    def playing_file_update(self, filename):
        if filename == "mplayer.":
            self.currently_playing_clip = None
            self.currently_playing_filename = None
            return
            
        self.currently_playing_filename = filename
        clips = self.clips(True)
        if clips == []:
            self.currently_playing_clip = None
        else:
            try:
                i = clips.index(self.currently_playing_clip)
            except ValueError:
                i = -1
            try:
                self.currently_playing_clip = clips[i+1]
            except IndexError:
                self.currently_playing_clip = clips[i]
                
    def receive_play_time(self, pos):
        if self.currently_playing_clip is not None:
            self.current_playpos = (
                pos - self.currently_playing_clip.start_float + 
                self.currently_playing_clip.time_line_offset)
            self.update_time_views()
                
    def update_time_views(self):
        data = (self.currently_playing_clip.id, self.current_playpos)
        for widget in self.time_view_widgets:
            widget.receive_time_data(data)
            
    def add_source_file(self, filename, use_as_clip=False):
        '''
        see if we already have a source node with this filename
        if we do.. all is well. otherwise - create it.
        schema is as follows
        <source location="~/Videos/examples.ogv">
            <clip/>
        </source>
        '''
        if filename in self.source_files:
            return        
        filename = filename.strip()
        source_type = None
        ftype, encoding =  mimetypes.guess_type(filename)

        for type_ in ("video", "audio", "image"):
            if ftype.startswith(type_):
                source_type = type_
                break
        if source_type is None:
            raise TypeError (filename)
        
        source_node = self.doc.createElement("source")
        source_node.setAttribute("location", filename)
        if ftype.startswith("image"):
            source_node.setAttribute("media-type", "image")
        
        self.doc.documentElement.appendChild(source_node)
        
        if use_as_clip:
            self.use_full_source_as_clip(filename)
        else:
            self.update_history()
    
    def use_full_source_as_clip(self, filename):
        '''
        add the source as a clip (no alteration to start pos or endpos)
        '''
        clip_node = self.doc.createElement("clip")
        clip_node.setAttribute("clip-id", self.next_clip_id)
        source_node = self.source_node_from_filename(filename.strip())
        source_node.appendChild(clip_node)
        
        self.update_history()
        
    def add_clip(self, filename, start, end):
        '''
        add a clip (filename must be known!)
        '''
        if filename not in self.source_files:
            print ("whoops.. clip filename not found!")
            print ("THIS SHOULDN'T HAPPEN!")
            return
        
        clip_node = self.doc.createElement("clip")
        clip_node.setAttribute("clip-id", self.next_clip_id)
        source_node = self.source_node_from_filename(filename.strip())
        source_node.appendChild(clip_node)
        clip_node.setAttribute("start", "%.1f"% start)
        clip_node.setAttribute("end", "%.1f"% end)
        
        self.update_history()
    
    def clip_from_selected(self, selected_clip):
        '''
        returns a tuple, (source, clip)
        '''
        source_node = self.source_node_from_filename(selected_clip.filename)
    
        clips = source_node.getElementsByTagName("clip")
        clip_node = None
        for clip in clips:
            if clip.getAttribute("clip-id") == selected_clip.id:
                clip_node = clip
        
        return source_node, clip_node
    
    def source_from_filename(self, filename):
        for source in self.sources:
            if source.filename == filename.strip():
                return source
    
    def split_clip(self, selected_clip, time_stamp):
        print("splitting clip", selected_clip, time_stamp)
        
        source_node, current_node = self.clip_from_selected(selected_clip)
        
        current_node.setAttribute("end", "%.1f"% time_stamp)
        
        new_clip_id = self.next_clip_id
        new_clip_node = self.doc.createElement("clip")
        new_clip_node.setAttribute("clip-id", new_clip_id)
        new_clip_node.setAttribute("start", "%.1f"% time_stamp)
        
        source_node.appendChild(new_clip_node)
        
        self.update_history()
        return new_clip_id
    
    def delete_clip(self, selected_clip):
        '''
        deletes the selected clip
        '''
        print("deleting clip", selected_clip) 
        source_node, clip_node = self.clip_from_selected(selected_clip)
        print(source_node.removeChild(clip_node))
        self.update_history()
    
    def source_node_from_filename(self, filename):
        '''
        returns the source_file node for a known filename
        '''
        l = []
        for source_node in self.doc.getElementsByTagName("source"):
            if source_node.getAttribute("location") == filename:
                return source_node
    
    def source_location_from_clipnode(self, clipnode):
        '''
        returns the file location for a known clipnode
        '''
        source_node = clipnode.parentNode
        return source_node.getAttribute("location")
    
    @property
    def number_of_sources(self):
        '''
        returns the current number of known sources
        '''
        return len(self.source_files)
    
    def remove_relative_paths(self, filename):
        '''
        auteur allows the user to use relative paths in a workbench
        (essential for project sharing)
        such a path is given relative to the project.xml file, and 
        stored in a node called "relative-path".
        example
        <relative-path root="." />
        this node must preceed all sources in the project file (as enforced
        by xsd validation
        '''
        path_nodes = self.doc.getElementsByTagName("relative-path")
        if path_nodes:
            location = path_nodes[0].getAttribute("root").strip()

            #change current directory in case root is of the form "."

            curdir = os.path.abspath(os.curdir)
            os.chdir(os.path.dirname(filename))
            base = os.path.abspath(location)
            
            #now change back
            os.chdir(curdir)
            
            for source_node in self.doc.getElementsByTagName("source"):
                loc = source_node.getAttribute("location")
                true_path = os.path.join(base, loc)
                source_node.setAttribute("location", true_path)
    
            self.doc.documentElement.removeChild(path_nodes[0])
    
    @property
    def source_files(self):
        '''
        returns a list of source_file locations
        eg. ["/home/user/Videos/holiday1.avi", ...]
        '''
        locations = []
        self.media_types = {}
        for source_node in self.doc.getElementsByTagName("source"):
            loc = source_node.getAttribute("location")
            self.media_types[loc] = source_node.getAttribute("media-type")
            locations.append(loc)
        return locations
    
    @property
    def total_clips(self):
        '''
        returns the current number of clips 
        (including those not added to timeline)
        '''
        return len(self.clips(False))        
    
    @property
    def number_of_clips(self):
        '''
        returns the current number of active clips
        '''
        return len(self.clips())
        
    def clips(self, timeline_only=True):
        '''
        returns a list of clips (in order!)
        '''
        if not self.is_dirty:
            try:
                return self._clips[timeline_only]
            except KeyError:
                pass
        clips = {}
        order = []
        for clip_node in self.doc.getElementsByTagName("clip"):
            in_timeline = clip_node.getAttribute("in-timeline").lower()!="false"
            if (not timeline_only or in_timeline):
                source = clip_node.parentNode
                if source.getAttribute("media-type") == "image":
                    clip = ImageClip()
                else:
                    clip = Clip()
                    
                clip.filename = source.getAttribute("location")
                clip._id = clip_node.getAttribute("clip-id")
                clip._start = clip_node.getAttribute("start")
                clip._end = clip_node.getAttribute("end")
                clip.in_timeline = in_timeline
                clips[clip._id] = clip
                order.append(clip._id)
            
        ## look for a custom clip order node.. if it doesn't exist, then
        ## use Alphabetical order ie. 0001 0002 0003
            
        clip_id_order = self.clip_id_order
        if clip_id_order is None:
            order.sort()
            clip_id_order = order
        else:
            for id in clip_id_order:
                if not id in order:
                    clip_id_order.remove(id)
            for id in order:
                if not id in clip_id_order:
                    clip_id_order.append(id)

        tl_pos = 0
        ordered_clips = []
        for clip_id in clip_id_order:
            clip = clips.get(clip_id, None)
            if clip is None:
                print ("WARNING - garbage found in clip-order node")
            else:
                clip.set_time_line_offset(tl_pos)
                tl_pos += clip.length
                ordered_clips.append(clip)
                
        self._clips[timeline_only] = ordered_clips
        
        return ordered_clips
    
    @property
    def duration(self):
        if self._duration is None:
            self._duration = 0
            for clip in self.clips():
                self._duration += clip.length
        return self._duration
        
    def order_node(self, create = False):
        '''
        returns the clip order node (creating one if necessary)
        '''
        order_nodes = self.doc.getElementsByTagName("clip-order")
        if order_nodes == []:
            if create:
                order_node = self.doc.createElement("clip-order")
                self.doc.documentElement.appendChild(order_node)
            else:
                order_node = None
        else:
            order_node = order_nodes[0]
        return order_node
        
    @property
    def clip_id_order(self):
        '''
        returns a list (text node) describing clip order
        eg ["0001", "0003", "0002"] or None if this node doesn't exist
        '''
        order_node = self.order_node()
        if order_node is None:
            return None
        
        order_string = order_node.firstChild.data
        order = order_string.replace("\n"," ").split(" ")
        
        while "" in order:
            order.remove("")
        
        return order
    
    def advance_clip(self, selected_clip, timeline_only=False):
        '''
        move a clip forward in the timeline
        '''
        print ("advance clip", selected_clip)
        clips = self.clips(timeline_only)[:]  #quick copy
        if selected_clip not in clips:
            print ("not found")
            return
        
        i = clips.index(selected_clip)
        clips.pop(i)
        order_string = ""
        for clip in clips[:i-1] + [selected_clip] + clips[i-1:]:
            order_string += "%s "% clip.id
        
        order_node = self.order_node(True)
        for child in order_node.childNodes:
            order_node.removeChild(child)
        
        text_node = self.doc.createTextNode(order_string.strip(" "))
        order_node.appendChild(text_node)
        
        self.update_history()
        
    def retreat_clip(self, selected_clip, timeline_only=False):
        '''
        move a clip backwards - TODO - LOTS OF REPEATED CODE HERE
        '''
        print ("retreat clip", selected_clip)
        clips = self.clips(timeline_only)[:]  #quick copy
        if selected_clip not in clips:
            print ("not found")
            return
        
        i = clips.index(selected_clip)
        clips.pop(i)
        order_string = ""
        for clip in clips[:i+1] + [selected_clip] + clips[i+1:]:
            order_string += "%s "% clip.id
        
        order_node = self.order_node(True)
        for child in order_node.childNodes:
            order_node.removeChild(child)
        
        text_node = self.doc.createTextNode(order_string.strip(" "))
        order_node.appendChild(text_node)
        
        self.update_history()
    
    def toggle_clip_in_timeline(self, selected_clip):
        '''
        receives a clip object, and toggles the boolean as to whether 
        it is in the timeline or not
        '''
        source_node, clip_node = self.clip_from_selected(selected_clip)
        clip_node.setAttribute("in-timeline", 
            str(not selected_clip.in_timeline).lower())
        self.update_history()
        
    def adjust_clip_times(self, clip, start, end):
        '''
        called by the clip adjusting dialog
        '''
        if start != clip.start_float or end != clip.end_float:
            source_node, clip_node = self.clip_from_selected(clip)
            clip_node.setAttribute("start", str(start))
            clip_node.setAttribute("end", str(end))
            
            self.update_history()
                
    @property
    def sources(self):
        '''
        returns a list of sources
        '''
        sources = []
        for source_file in self.source_files:
            if self.media_types.get(source_file) == "image": 
                source = ImageSource()
            else:
                source = Source()
            source.filename = source_file
                    
            sources.append(source)
            
        return sources

    @property
    def xml(self):
        '''
        returns the current xml - including a hack to ensure that the
        node sequencing is correct.
        xs:sequence enforces that the order is
        <auteur><source/><source/><clip-order/></auteur>
        '''
        order_node = self.order_node()
        if order_node is not None:
            self.doc.documentElement.removeChild(order_node)
            self.doc.documentElement.appendChild(order_node)
        return self.doc.toxml()
    
    @property
    def pretty_xml(self):
        '''
        human readable form of the html
        '''
        return self._beautified_xml(self.xml)
    
    def _beautified_xml(self, xml):
        '''
        NOTE - minidom's pretty xml function introduces WAY too much 
        whitespace if any text nodes are present.
        hence jump through a hoop a little here....
        also.. I will eventualy want attributes in this order
        clip-id, start, end..
        but that's on the TODO list
        '''
        pretty = re.sub(">[ \n\t]*<", "><", xml)
        pretty = pretty.replace("><",">\n<")
        pretty = pretty.replace("\n<source","\n\t<source")
        pretty = pretty.replace("\n</source","\n\t</source")
        pretty = pretty.replace("\n<clip","\n\t\t<clip")
        
        return pretty
    
    def _restore_last_history(self):
        doc = minidom.parseString(self.history[-1])
        self.doc = doc
        self.update_history()
        self.update_views()
        
    def undo(self):
        if self.undo_available:
            self.redo_list.append(self.history.pop(-1))
            self._restore_last_history()
            
    def redo(self):
        if self.redo_available:
            self.history.append(self.redo_list.pop(-1))
            self._restore_last_history()
            
    @property
    def xml_widget_text(self):
        '''
        the text supplied to the xml_widget
        '''
        return self.pretty_xml
    
    @property
    def biggest_source_dimension(self):
        '''
        finds the tallest source video, returns it's dimensions
        '''
        still_width, still_height = 0, 0
        width, height = 0, 0
        for source in self.sources:
            if source.media_type == source.IMAGE_MEDIA:
                if still_height < source.height:
                    still_height = source.height
                    still_width = source.width
            else:
                if height < source.height:
                    height = source.height
                    width = source.width
        
        if (width, height) ==( 0, 0):
            return "%s:%s"% (still_width, still_height)
        return "%s:%s"% (width, height)
        
    @property
    def highest_framerate(self):
        framerate = 0
        for source in self.sources:
            if source.media_type == source.VIDEO_MEDIA:
                if source.framerate > framerate:
                    framerate = source.framerate
        return "25" if framerate == 0 else str(framerate)
        
    @property
    def stills(self):
        stills = []
        for source in self.sources:
            if source.media_type == source.IMAGE_MEDIA:
                stills.append(source)
        return stills
        
    def mencoder_command_list(self, outfile=None):
        '''
        the current mencoder_commands for the data
        
        based on this suggestion from auteur founder and advocate, klaatu 

        mencoder evildead.ogv -ss 00:13:13 -endpos 00:00:13 -oac pcm -ovc lavc 
        armyofdarkness.ogv -ss 00:66:06 -endpos 00:06:06 -oac pcm -ocv lavc -o 
        ./render/evilcomposite.avi
                
        UPDATED VERSION 2011-01-11
        mencoder foo.flv -ss 00:00:03 -endpos 43 -oac pcm -ofps 24 -ovc lavc 
        -vf scale=320:240 bar.ogv -ss 00:01:23 -endpos 12 -oac pcm -ofps 24 
        -ovc lavc -vf scale=320:240 -o baz.avi
        
        
        after experimentation - no need to duplicate all this stuff for one
        clip.
        '''        
        
        #######################################################################
        ##  I can not overemphasize the importance of this function          ##
        ## this determines the output.                                       ##
        ## It would make sense to allow the (advanced) user to choose the    ##
        ## following                                                         ##
        ## 1. framerate                                                      ##
        ## 2. scale                                                          ##
        ## 3. audio codec                                                    ##
        ## 4. video codec                                                    ##
        ## at the moment, auteur makes compromises. if all the sources are   ##
        ## the same (and are all videos) then framerate needn't be set       ##
        ## and oac and ovc should be set to "copy" (or left blank?           ##
        ## however, if one does set these.. any variation in source type     ##
        ## will cause the render to fail                                     ##
        #######################################################################
        
        
        if outfile is None:
            outfile = "[YOUR_OUTFILE]"
        
        commands = [    "mencoder", 
                        "-o", outfile, 
                        "-vf", "scale=%s"% self.biggest_source_dimension ,
                        "-ofps", "%s" %self.highest_framerate,
                        ]                    
        
        commands += ["-oac", "pcm" ,
                        "-ovc", "lavc"] 
            
                    
        insert_point = len(commands)
        for clip in self.clips():
            
            if clip.media_type == clip.IMAGE_MEDIA:
                fps = 1/clip.length
                if fps < 0.01:
                    fps = 0.01
                commands += ["mf://%s"% clip.filename,  "-mf", 
                            "fps=%.2f"% fps, 
                            "-audiofile", 
                            "/usr/share/auteur/auteur_silence.oga"]
            else:
                commands += [clip.filename]
                if clip.start != "":
                    commands += ["-ss", clip.start_format]
                if clip.end != "":
                    commands += ["-endpos", clip.end_format]
    
                        
    
        return commands, insert_point
    
    def escaped_command(self, command):
        for bad_char in (" ", ";"):
            escape_char = "\%s"% bad_char
            command = command.replace(bad_char, escape_char)
        return command
    
    @property
    def mencoder_string(self):
        mencoder_command = ""
        commands, insert_point = self.mencoder_command_list()
        commands = commands[:insert_point] + ["[OPTIONS]"] + commands[insert_point:]
        for command in commands:
            mencoder_command += self.escaped_command(command) + " "
        
        return mencoder_command
    
    @property
    def preview_commands(self):
        '''
        the current mencoder_commands for the data
        '''
        
        commands = [    "-nolirc",
                        "-vf", "scale=%s"% self.biggest_source_dimension 
                    ]
                    
        for clip in self.clips():
            commands += clip.preview_command_list
                
        return commands
    

KNOWN_IMAGES, KNOWN_PROPERTIES = {}, {}

def _test_model():
    import gettext
    gettext.install("auteur")

    app = QtGui.QApplication([])
    import sys
    sys.path.insert(0, os.path.abspath("../"))
    from lib_auteur.components import q_resources
    
    obj = DataModel() 
    obj.load("test_project/test_project.xml")
    
    print(obj.pretty_xml)
    print("file duration %s seconds"% obj.sources[0].length)
    print (obj.mencoder_string)
    app=QtCore.QCoreApplication([])
    obj.validate(obj.xml)
    
    clip = obj.clips()[1]
    print (obj.xml)
    obj.advance_clip(clip)
    print (obj.xml)
    print (obj.stills)
    
if __name__ == "__main__":
    _test_model()

#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division

import re, subprocess, sys, time
from PyQt4 import QtGui, QtCore

if __name__ == "__main__":
    import os
    sys.path.insert(0, os.path.abspath("../"))

from lib_auteur.components import DataModel
from lib_auteur.settings import QAuteurSettings
from lib_auteur.components import MencoderWidget

class RendererWindow(QtGui.QMainWindow):
    def __init__(self, project, outfile, mencoder_opts, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
           
        self.setWindowTitle("auteur - rendering with mencoder")
        self.setWindowIcon(QtGui.QIcon(":images/handInFilm.svg"))
        
        self.project = project
        self.outfile = outfile
        self.mencoder_opts = mencoder_opts
        
        self.model = DataModel()
        
        self.label =  QtGui.QLabel()
        self.label.setWordWrap(True)
        
        outfile_but = QtGui.QPushButton("...")
        outfile_but.setFixedSize(30,30)
        outfile_but.clicked.connect(self.get_outfile)
        
        self.choose_file_frame = QtGui.QFrame()
        layout = QtGui.QHBoxLayout(self.choose_file_frame)
        layout.addWidget(self.label)
        layout.addWidget(outfile_but)
        
        self.mencoder_widget = MencoderWidget()
        self.mencoder_widget.show_label(False)
        self.mencoder_widget.set_model(self.model)
        self.mencoder_widget.text_browser.setReadOnly(False)
        
        label = QtGui.QLabel(_("Edit with care - changes WILL apply!"))
        self.command_frame = QtGui.QFrame()
        layout = QtGui.QVBoxLayout(self.command_frame)
        layout.addWidget(self.mencoder_widget)
        layout.addWidget(label)
        
        button_box = QtGui.QDialogButtonBox(self)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        
        self.cancel_button = button_box.addButton(button_box.Cancel)        
        self.cancel_button.clicked.connect(self.cancel_process)
        
        self.apply_button = button_box.addButton(button_box.Apply) 
        self.apply_button.setText(_("RENDER"))       
        self.apply_button.clicked.connect(self.render)        
        
        icon = QtGui.QIcon.fromTheme("go-down")
        more_but = QtGui.QPushButton(icon, "&Advanced")
        more_but.setFlat(True)

        more_but.setCheckable(True)
        more_but.setFocusPolicy(QtCore.Qt.NoFocus)
        more_but.clicked.connect(self.advanced)
        button_box.addButton(more_but, button_box.HelpRole)
        
        self.render_frame = QtGui.QFrame()
        label = QtGui.QLabel(_("Rendering... please wait!"))
        self.pb = QtGui.QProgressBar()
        
        layout = QtGui.QVBoxLayout(self.render_frame)
        layout.addWidget(label)
        layout.addWidget(self.pb)
        
        splitter = QtGui.QSplitter()
        splitter.setOrientation(QtCore.Qt.Vertical)
        splitter.addWidget(self.choose_file_frame)
        splitter.addWidget(self.command_frame)
        splitter.addWidget(self.render_frame)
        splitter.addWidget(button_box)
        
        #self.apply_button.setEnabled(self.outfile!="")        
        self.command_frame.hide()        
        self.render_frame.hide()
        self.cancel_button.hide()
        
        self.setCentralWidget(splitter)
        self.set_label_text()
        
        self.settings = QAuteurSettings(self)
        
        self.load_settings()
        self.number_of_clips = 1
        QtCore.QTimer.singleShot(0, self.load_proj)

    def advanced(self):
        QtGui.QMessageBox.information(self, _("TODO"), 
            _("No Advanced Features (Yet)"))

    def load_settings(self):
        try:
            self.restoreState(self.settings.window_state)
            self.restoreGeometry(self.settings.geometry)
        except KeyError:
            print ("no previous window state found")
    
    def save_settings(self):
        self.settings.setValue("docked_state", self.saveState())
        self.settings.setValue("docked_geometry", self.saveGeometry())
    
    def closeEvent(self, event=None):
        '''
        re-implement the close event of QtGui.QMainWindow, and check the user
        really meant to do this.
        '''
        self.save_settings()
        
    def get_outfile(self):
        outfile = QtGui.QFileDialog.getSaveFileName(self, 
        "Please choose an output filename and location",
        "output.avi")
        if outfile != "":
            self.outfile = outfile
            self.mencoder_widget.set_outfile(outfile)
            self.set_label_text()
            self.apply_button.setEnabled(True)
            self.command_frame.show()
            
    def load_proj(self):
        try:
            self.model.load(self.project)       
        except IOError as e:
            QtGui.QMessageBox.warning(self, "FATAL", "%s<hr />%s"%(
            "There is a problem with your control file", e))
            return
        
        self.number_of_clips = self.model.number_of_clips
        
        self.mencoder_widget.set_options(self.mencoder_opts)
        if self.outfile:
            self.mencoder_widget.set_outfile(self.outfile)
            self.apply_button.setEnabled(True)
            self.command_frame.show()
            
        else:
            self.apply_button.setEnabled(False)
            
    def set_label_text(self):
        if self.outfile:
            message = "%s %s - <b>'%s'</b>"% (
        _('Auteur is ready to send the following command'),
        _('which will render your video to the following location'),
        self.outfile)
        else:
            message = _("Please choose an outfile")
            
        self.label.setText(message)
        
    def sizeHint(self):
        return QtCore.QSize(400,200)
        
    def cancel_process(self):
        try:
            self.process.terminate()
        except AttributeError:
            pass
        QtGui.QApplication.instance().closeAllWindows() 
        
    def render(self):
        '''
        ok.. render the video
        ''' 
        self.choose_file_frame.hide()
        self.render_frame.show()
        self.cancel_button.show()
        self.apply_button.hide()
        self.command_frame.setEnabled(False)
               
        commands = self.mencoder_widget.command_list
        print (commands)
        self.process = subprocess.Popen(commands, 
                                stdout=subprocess.PIPE, 
                                stderr=subprocess.STDOUT)
        clip_no = -1
        while True:
            QtGui.QApplication.instance().processEvents()
            bytes = self.process.stdout.readline()
            out = str(bytes).strip("\r")
            #grep for Pos:  21.5s    537f (100%) 87.87fps 
            
            if bytes == b"" or out.startswith("Exiting"):
                print (out)
                break
            
            m = re.search("Pos:.*\((\d+)%\)", out)
            if m:
                pos = int(m.groups()[0])  # a percentage
                pos = (clip_no*100 +pos) / self.number_of_clips
                self.pb.setValue(pos)
            else:
                if "VIDEO" in out:
                    clip_no +=1
            print ("OUT:%s"% out)
        
        self.process.wait()
        self.pb.setValue(100)
            
        print("finished rendering")
        QtGui.QMessageBox.information(self,"message",
            "rendering process complete")
        
        QtGui.QApplication.instance().closeAllWindows()
    
def main(project, outfile=None, mencoder_opts=""):
    app = QtGui.QApplication([])
    app.setOrganizationName("q-auteur")
    app.setApplicationName("renderer")
    
    opts = mencoder_opts.split(" ")
    while "" in opts:
        opts.remove("")
    mw = RendererWindow(project, outfile, opts)
    mw.show()
    app.exec_()
    
def _test():
    import gettext
    gettext.install("auteur")
     
    main("test_project/test_project.xml")
    
if __name__ == "__main__":
    _test()
    
#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

'''
Standard setup.py script for the auteur video editor
'''

from distutils.core import setup

import re, os

if os.path.isfile("MANIFEST"):
    os.unlink("MANIFEST")

# pull the major version from this git generated file
f = open("VERSION.txt", "r")
VERSION = re.sub("-.*","",f.read())
f.close()

setup(
    name = 'auteur',
    version = VERSION,
    description = 'Lightweight video editor powered by mplayer and mencoder',
    author = 'Neil Wallace',
    author_email = 'rowinggolfer@googlemail.com',
    url = 'http://auteur-editor.info',
    license = 'GPL v3',
    package_dir = {'lib_auteur' : 'src/lib_auteur'},
    packages = ['lib_auteur',
                'lib_auteur.components',
                'lib_auteur.components.resources',],
    data_files = [
        ('/usr/share/icons/hicolor/48x48/apps', ['bin/48x48/auteur.png']),
        ('/usr/share/icons/hicolor/64x64/apps', ['bin/64x64/auteur.png']),
        ('/usr/share/icons/hicolor/96x96/apps', ['bin/96x96/auteur.png']),
        ('/usr/share/icons/hicolor/128x128/apps', ['bin/128x128/auteur.png']),
        ('/usr/share/icons/hicolor/scalable/apps', ['bin/auteur.svg']),
        ('/usr/share/applications', ['bin/Auteur.desktop']),
        ('/usr/share/auteur', ['bin/auteur_silence.oga'])
        ],
    scripts = ['src/auteur.py', 'src/auteur'],
    )
